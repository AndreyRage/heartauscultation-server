package ua.org.horishniy.heartauscultation.server.exceptions;

/**
 * Created by rage on 05.04.16.
 */
public class BadRequestException extends RuntimeException {
	public BadRequestException(String message) {
		super(message);
	}
}
