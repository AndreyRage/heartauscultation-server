package ua.org.horishniy.heartauscultation.server.enums;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by rage on 09.03.16.
 */
public enum Role implements GrantedAuthority {
	ADMIN, MODERATOR, DOCTOR, PATIENT;

	@Override
	public String getAuthority() {
		return name();
	}
}
