package ua.org.horishniy.heartauscultation.server.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ua.org.horishniy.heartauscultation.server.entities.HealthNote;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.exceptions.NotFoundException;
import ua.org.horishniy.heartauscultation.server.service.HealthNoteService;
import ua.org.horishniy.heartauscultation.server.service.UserService;
import ua.org.horishniy.heartauscultation.server.service.entities.requests.OkResponse;

import java.io.IOException;
import java.util.List;

/**
 * Created by rage on 06.04.16.
 */
@Controller
public class HealthNoteController {

	@Autowired
	public HealthNoteService mHealthNoteService;

	@Autowired
	public UserService mUserService;

	@RequestMapping(value = {"api/note/my"}, method = RequestMethod.GET)
	public ResponseEntity getHealthNotes(@RequestParam(value = "sinceId", required = false) Long sinceId,
										 @RequestParam(value = "beforeId", required = false) Long beforeId,
										 @RequestParam(value = "search", required = false) String search,
										 @RequestParam(value = "limit", required = false) Integer limit) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		return getHealthNotes(sinceId, beforeId, search, user.getId(), limit);
	}

	@RequestMapping(value = {"api/note"}, method = RequestMethod.GET)
	public ResponseEntity getHealthNotes(@RequestParam(value = "sinceId", required = false) Long sinceId,
										 @RequestParam(value = "beforeId", required = false) Long beforeId,
										 @RequestParam(value = "search", required = false) String search,
										 @RequestParam(value = "userId", required = false) Long userId,
										 @RequestParam(value = "limit", required = false) Integer limit) {
		List<HealthNote> list = mHealthNoteService.getHealthNotes(sinceId, beforeId, search, userId, limit);
		return ResponseEntity.ok(list);
	}

	@RequestMapping(value = {"api/note/{id}"}, method = RequestMethod.GET)
	public ResponseEntity getHealthNote(@PathVariable("id") long id) {
		HealthNote note = mHealthNoteService.get(id);
		if (note == null) {
			throw new NotFoundException("Health note not found");
		}
		return ResponseEntity.ok(note);
	}

	/*@RequestMapping(value = {"api/note"}, method = RequestMethod.PUT)
	public ResponseEntity addHealthNote(@RequestBody HealthNote note) {
		Long id = mHealthNoteService.add(note);
		if (!id.equals(-1L)) {
			return ResponseEntity.status(HttpStatus.CREATED).body(note);
		} else {
			return ResponseEntity.ok(note);
		}
	}*/

	@RequestMapping(value = {"api/note"}, method = RequestMethod.POST, consumes = {"multipart/form-data"})
	public ResponseEntity addHealthNoteAudio(@RequestParam(value = "file", required = false) MultipartFile file,
											 @RequestParam(value = "note") String note) throws IOException {
		HealthNote healthNote = new ObjectMapper().readValue(note, HealthNote.class);
		Long id = mHealthNoteService.add(healthNote, file);
		if (!id.equals(-1L)) {
			return ResponseEntity.status(HttpStatus.CREATED).body(note);
		} else {
			return ResponseEntity.ok(note);
		}
	}

	@RequestMapping(value = {"api/note/{id}"}, method = RequestMethod.DELETE)
	public ResponseEntity deleteHealthNote(@PathVariable("id") long id) {
		mHealthNoteService.delete(id);
		return ResponseEntity.ok(new OkResponse("Health note delete"));
	}
}
