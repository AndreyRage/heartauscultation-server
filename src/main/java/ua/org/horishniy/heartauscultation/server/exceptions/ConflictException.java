package ua.org.horishniy.heartauscultation.server.exceptions;

/**
 * Created by rage on 05.04.16.
 */
public class ConflictException extends RuntimeException {
	public ConflictException(String message) {
		super(message);
	}
}
