<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 26.03.16
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/navbar.jsp"/>
<div class="container">
    <div class="center-content-style">
        <div class="content-style jumbotron">
            <h2>${code}: ${message}</h2>
            <c:if test="${code == 404}">
                <p>The request was invalid or cannot be otherwise served.</p>
            </c:if>
        </div>
    </div>
</div>
<jsp:include page="include/footer.jsp"/>
