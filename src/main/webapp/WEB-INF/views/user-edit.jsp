<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 26.03.16
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/navbar.jsp"/>
<div class="container">
    <div class="center-content-style">
        <div class="container-body">
            <h2 class="container-title">Edit profile</h2>
            <form class="form-user-edit form-style">
                <div id="user-alert"></div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Username:</div>
                        <div class="col-sm-8 no-padding"><p class="form-control disabled">${user.getUsername()}</p></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">First name:</div>
                        <div class="col-sm-8 no-padding">
                            <label for="firstname" class="sr-only">First name</label>
                            <input type="text" id="firstname" class="form-control" name="firstname"
                                   placeholder="First name" value="${user.getFirstName()}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Last name:</div>
                        <div class="col-sm-8 no-padding">
                            <label for="lastname" class="sr-only">Last name</label>
                            <input type="text" id="lastname" class="form-control" name="lastname"
                                   placeholder="Last name" value="${user.getLastName()}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Email:</div>
                        <div class="col-sm-8 no-padding">
                            <label for="email" class="sr-only">Email</label>
                            <input type="text" id="email" class="form-control" name="email"
                                   placeholder="Email" value="${user.getEmail()}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Birthday:</div>
                        <div class="col-sm-8 no-padding">
                            <label for="birthday" class="sr-only">Last name</label>
                            <input type="text" class="form-control" value="<joda:format pattern="MM/dd/yyyy"
                                   value="${user.getBirthday()}"/>" name="birthday" id="birthday"
                                   placeholder="Date">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-9 no-padding"></div>
                        <div class="col-sm-3">
                            <button class="btn btn-lg btn-success btn-block" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <sec:authorize access="hasRole('DOCTOR')">
            <div class="container-body">
                <h2 class="container-title">Experience</h2>
                <form class="form-experience-edit form-style">
                    <div id="experience-alert"></div>
                    <div id="experience-group">
                        <c:forEach var="experience" items="${experiences}">
                            <div class="form-group experience-group-item">
                                <div class="row">
                                    <div class="col-sm-11">
                                        <input type="hidden" class="experience-id" name="experience-id" value="${experience.getId()}"/>
                                        <div class="row">
                                            <div class="col-sm-1 no-padding">Name:</div>
                                            <div class="col-sm-11 no-padding">
                                                <input type="text" class="form-control" name="name"
                                                       placeholder="Last name" value="${experience.getName()}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 no-padding text-center"></div>
                                            <div class="col-sm-1 no-padding">From:</div>
                                            <div class="col-sm-2 no-padding">
                                                <input type="text" class="form-control" name="fromYear"
                                                       placeholder="Year" value="${experience.getFromYear() != 0 ? experience.getFromYear() : ''}">
                                            </div>
                                            <div class="col-sm-1 no-padding">
                                                <input type="text" class="form-control" name="fromMonth"
                                                       placeholder="M" value="${experience.getFromMonth() != 0 ? experience.getFromMonth() : ''}">
                                            </div>
                                            <div class="col-sm-1 no-padding">
                                                <input type="text" class="form-control" name="fromDay"
                                                       placeholder="D" value="${experience.getFromDay() != 0 ? experience.getFromDay() : ''}">
                                            </div>
                                            <div class="col-sm-1 no-padding text-center">-</div>
                                            <div class="col-sm-1 no-padding">To:</div>
                                            <div class="col-sm-2 no-padding">
                                                <input type="text" class="form-control" name="toYear"
                                                       placeholder="Year" value="${experience.getToYear() != 0 ? experience.getToYear() : ''}">
                                            </div>
                                            <div class="col-sm-1 no-padding">
                                                <input type="text" class="form-control" name="toMonth"
                                                       placeholder="M" value="${experience.getToMonth() != 0 ? experience.getToMonth() : ''}">
                                            </div>
                                            <div class="col-sm-1 no-padding">
                                                <input type="text" class="form-control" name="toDay"
                                                       placeholder="D" value="${experience.getToDay() != 0 ? experience.getToDay() : ''}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="btn btn-default drug-group-delete experience-group-delete">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-1 no-padding"></div>
                            <div class="col-sm-11">
                                <a id="experience-group-button-add" class="btn btn-default drug-group-delete">Add experience</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-9 no-padding"></div>
                            <div class="col-sm-3">
                                <button class="btn btn-lg btn-success btn-block" type="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </sec:authorize>

        <div class="container-body">
            <h2 class="container-title">Change password</h2>
            <form class="form-password-edit form-style">
                <div id="password-alert"></div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Current password:</div>
                        <div class="col-sm-8 no-padding">
                            <label for="current-password" class="sr-only">Current password</label>
                            <input type="password" id="current-password" class="form-control" name="current-password"
                                   placeholder="Current password">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">New password:</div>
                        <div class="col-sm-8 no-padding">
                            <label for="new-password" class="sr-only">New password</label>
                            <input type="password" id="new-password" class="form-control" name="new-password"
                                   placeholder="New password">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Confirm the password:</div>
                        <div class="col-sm-8 no-padding">
                            <label for="confirm-password" class="sr-only">Confirm the password</label>
                            <input type="password" id="confirm-password" class="form-control" name="confirm-password"
                                   placeholder="Confirm the password">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-9 no-padding"></div>
                        <div class="col-sm-3">
                            <button class="btn btn-lg btn-warning btn-block" type="submit">Change</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var currentUserEmail = "${user.getEmail()}";
    var currentUserFirstName = "${user.getFirstName()}";
    var currentUserLastName = "${user.getLastName()}";
    var currentUserBirthday = "${user.getBirthday()}";
</script>

<script src="<c:url value='/static/jquery/jquery-1.12.2.min.js' />"></script>
<script>window.jQuery || document.write('<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"><\/script>')</script>
<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"></script>
<script src="<c:url value='/static/datepicker/js/bootstrap-datepicker.js' />"></script>
<script src="<c:url value='/static/js/user-edit.js' />"></script>
<script src="<c:url value='/static/js/utils.js' />"></script>

<jsp:include page="include/footer.jsp"/>
