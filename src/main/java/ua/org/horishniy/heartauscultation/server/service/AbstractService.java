package ua.org.horishniy.heartauscultation.server.service;

import org.springframework.transaction.annotation.Transactional;
import ua.org.horishniy.heartauscultation.server.dao.BaseDao;

import java.io.Serializable;

@Transactional
public abstract class AbstractService<PK extends Serializable, T> implements BaseService<PK, T> {

	private BaseDao<PK, T> mDao;

	AbstractService(BaseDao<PK, T> dao){
		this.mDao = dao;
	}

	public BaseDao<PK, T> getDao() {
		return mDao;
	}

	@Override
	public T get(PK id) {
		if (id != null) {
			return getDao().get(id);
		}
		return null;
	}

	@Override
	public PK add(T object) {
		if (isValid(object)) {
			return getDao().add(object);
		}
		return null;
	}

	@Override
	public void update(T object) {
		if (isValid(object)) {
			getDao().update(object);
		}
	}

	@Override
	public void delete(T object) {
		if (object != null) {
			getDao().delete(object);
		}
	}

	@Override
	public void delete(PK id) {
		if (id != null) {
			getDao().delete(id);
		}
	}
}