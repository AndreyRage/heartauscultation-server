/**
 * Created by rage on 31.03.16.
 */

$(function () {
    $("form.form-add-note").submit(function (e) {
        e.preventDefault();
        doAddNote();
    });

    $("#btn-delete-file").click(function (e) {
        e.preventDefault();
        deleteNoteFile();
    });

    $("#btn-delete-note").click(function (e) {
        e.preventDefault();
        doDeleteNote();
    });

    initDrug();
});

function initDrug() {
    var max_fields = 10;
    var wrapper = $("#drug-group");

    var x = $(".drug-group-item").length;

    $("#drug-group-button-add").click(function (e) {
        e.preventDefault();
        if (x < max_fields) {
            addField();
        }
    });

    $(wrapper).on("click", ".drug-group-delete", function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        x--;
        if (x == 0) {
            addField();
        }
    });

    function addField() {
        x++;
        $(wrapper).append(
            '<div class="row no-padding drug-group-item"> <div class="col-sm-6 no-padding"> <input type="text" class="form-control" name="drug-name" placeholder="Name"> </div> <div class="col-sm-2 no-padding"> <input type="number" step="0.01" class="form-control" name="drug-dosage" placeholder="Dosage"> </div> <div class="col-sm-2 no-padding"> <select class="form-control center-field form-select" name="drug-measure" title="Dosage"> <option></option> <option value="PCS">pcs</option> <option value="KILO_G">kg</option> <option value="G">g</option> <option value="MILLI_G">mg</option> <option value="MICRO_G">µg</option> <option value="NANO_G">ng</option> <option value="L">l</option> <option value="MILLI_L">ml</option> <option value="MICRO_L">µl</option> <option value="NANO_L">nl</option> </select> </div> <div class="col-sm-2 no-padding"> <a class="btn btn-default drug-group-delete">Delete</a> </div> <input type="hidden" class="drug-id" name="drug-id" value=""/> </div>'
        );
    }
}

function deleteNoteFile() {
    $("#file-content").html('<label for="diastolic" class="sr-only">File</label><input type="file" id="file" name="file" placeholder="File" value="">');
}

function showNoteError(message) {
    if (message == null) {
        $("#note-alert").html('');
    } else {
        $("#note-alert").html('<div class="alert alert-danger"><p>' + message + '</p></div>');
    }
}

function doAddNote() {
    var fId = $("#id");
    var fSystolic = $("#systolic");
    var fDiastolic = $("#diastolic");
    var fActivity = $("input[name=activity]:checked");
    var fDescription = $("#description");
    var fCurrentFile = $("#current-file");
    var fFile = $("#file");

    var file;
    var currentFile;
    if (fFile.length > 0) {
        file = fFile[0].files[0];
    } else if (fCurrentFile.val().length != "") {
        currentFile = fCurrentFile.val();
    }

    var noteForm = {
        id: fId.val(),
        recFile: currentFile,
        systolic: parseInt(fSystolic.val()),
        diastolic: parseInt(fDiastolic.val()),
        drugs: [],
        action: fActivity.val(),
        description: fDescription.val()
    };

    var isError = false;
    $('#drug-group .drug-group-item').each(function () {
        var drugForm = {
            id: $("input[name='drug-id']", this).val(),
            name: $("input[name='drug-name']", this).val(),
            dosage: $("input[name='drug-dosage']", this).val(),
            measure: $("select[name='drug-measure']", this).val()
        };
        if (drugForm.id == "") {
            drugForm.id = null;
        }
        if (drugForm.measure == "" || drugForm.dosage == "") {
            drugForm.measure = null;
        }
        if (drugForm.dosage != "") {
            if (parseFloat(drugForm.dosage) == 0) {
                showNoteError("Measure can not be zero");
                isError = true;
                return;
            }
            if (parseFloat(drugForm.dosage) < 0) {
                showNoteError("Measure can not be negative");
                isError = true;
                return;
            }
        }
        if (drugForm.name != "" || drugForm.dosage != "") {
            noteForm.drugs.push(drugForm);
        }
    });

    if (isError) {
        return;
    }

    if (isNaN(noteForm.systolic) != isNaN(noteForm.diastolic)) {
        showNoteError("Wrong blood pressure: You need to fill both fields or leave them blank");
        return;
    }

    if (!isNaN(noteForm.systolic) && !isNaN(noteForm.systolic)
        && noteForm.diastolic > noteForm.systolic) {
        showNoteError("Systolic can't be less then diastolic");
        return;
    }

    if (isNaN(noteForm.systolic) && isNaN(noteForm.diastolic)
        && noteForm.drugs.length === 0
        && file == null && fFile.length > 0
        && noteForm.description == "") {
        showNoteError("At least one field must be filled");
        return;
    }

    var formData = new FormData();
    formData.append("note", JSON.stringify(noteForm));

    if (file != null) {
        formData.append("file", file);
    }

    $.ajax({
        url: '/api/note',
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function () {
            goToHealthNotes();
        },
        error: function (xhr, status, error) {
            showNoteError(handleError(xhr, status));
        }
    });
}

function doDeleteNote() {
    var id = $("#id").val();
    $.ajax({
        url: '/api/note/' + id,
        type: "DELETE",
        success: function () {
            goToHealthNotes();
        },
        error: function (xhr, status, error) {
            showNoteError(handleError(xhr, status));
        }
    });
}

function goToHealthNotes() {
    if (document.referrer.indexOf('/health-notes') > -1) {
        window.history.back();
    } else {
        window.location.replace("/health-notes");
    }
}