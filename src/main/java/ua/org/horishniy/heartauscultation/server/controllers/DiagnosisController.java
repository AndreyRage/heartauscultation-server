package ua.org.horishniy.heartauscultation.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.org.horishniy.heartauscultation.server.entities.Diagnosis;
import ua.org.horishniy.heartauscultation.server.service.DiagnosisService;
import ua.org.horishniy.heartauscultation.server.service.entities.requests.OkResponse;

import java.util.List;

/**
 * Created by rage on 09.04.16.
 */
@Controller
public class DiagnosisController {

	@Autowired
	public DiagnosisService mDiagnosisService;

	@RequestMapping(value = {"api/note/{id}/diagnosis"}, method = RequestMethod.GET)
	public ResponseEntity getDiagnosis(@PathVariable("id") long id,
									   @RequestParam(value = "sinceId", required = false) Long sinceId,
									   @RequestParam(value = "beforeId", required = false) Long beforeId,
									   @RequestParam(value = "limit", required = false) Integer limit) {
		List<Diagnosis> diagnosis = mDiagnosisService.getDiagnosis(sinceId, beforeId, id, limit);
		return ResponseEntity.ok(diagnosis);
	}

	@RequestMapping(value = {"api/note/{id}/diagnosis"}, method = RequestMethod.POST)
	public ResponseEntity addDiagnosis(@PathVariable("id") long id, @RequestBody Diagnosis diagnosis) {
		diagnosis.setHealthNoteId(id);
		mDiagnosisService.add(diagnosis);
		return ResponseEntity.status(HttpStatus.CREATED).body(diagnosis);
	}

	@RequestMapping(value = {"api/diagnosis/{id}"}, method = RequestMethod.PUT)
	public ResponseEntity editDiagnosis(@PathVariable("id") long id, @RequestBody Diagnosis diagnosis) {
		diagnosis.setId(id);
		mDiagnosisService.update(diagnosis);
		return ResponseEntity.ok(diagnosis);
	}

	@RequestMapping(value = {"api/diagnosis/{id}"}, method = RequestMethod.DELETE)
	public ResponseEntity deleteDiagnosis(@PathVariable("id") long id) {
		mDiagnosisService.delete(id);
		return ResponseEntity.ok(new OkResponse("Diagnosis delete"));
	}
}
