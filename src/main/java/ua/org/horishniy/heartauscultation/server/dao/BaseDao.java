package ua.org.horishniy.heartauscultation.server.dao;

import java.io.Serializable;

/**
 * Created by rage on 09.03.16.
 */
public interface BaseDao<PK extends Serializable, T> {
	T get(PK id);
	PK add(T object);
	void update(T object);
	void merge(T object);
	void delete(T object);
	void delete(PK id);
}
