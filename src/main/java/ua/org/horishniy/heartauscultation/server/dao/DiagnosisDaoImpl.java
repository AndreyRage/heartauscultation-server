package ua.org.horishniy.heartauscultation.server.dao;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ua.org.horishniy.heartauscultation.server.entities.Diagnosis;

import java.util.List;

/**
 * Created by rage on 09.03.16.
 */
@Repository("diagnosisDao")
public class DiagnosisDaoImpl extends AbstractDao<Long, Diagnosis> implements DiagnosisDao {
	private static final int LIMIT = 10;

	public DiagnosisDaoImpl() {
		super(Diagnosis.class);
	}

	@Override
	public List<Diagnosis> getBeforeId(@Nullable Long id, int limit) {
		return getBeforeId(id, null, limit);
	}

	@Override
	public List<Diagnosis> getSinceId(@Nullable Long id, int limit) {
		return getSinceId(id, null, limit);
	}

	@Override
	public List<Diagnosis> getBeforeId(@Nullable Long id, @Nullable Long noteId, int limit) {
		return createSqlQuery(true, id, noteId, limit);
	}

	@Override
	public List<Diagnosis> getSinceId(@Nullable Long id, @Nullable Long noteId, int limit) {
		return createSqlQuery(false, id, noteId, limit);
	}

	@SuppressWarnings("unchecked")
	private List<Diagnosis> createSqlQuery(boolean before, @Nullable Long id, @Nullable Long noteId, int limit) {
		if (limit == 0) {
			limit = LIMIT;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM (SELECT * FROM DIAGNOSIS");
		if (id != null || noteId != null) {
			sb.append(" WHERE");
		}
		if (id != null) {
			sb.append(" ID");
			sb.append(before ? " < " : " > ");
			sb.append(id);
		}
		if (noteId != null) {
			if (id != null) {
				sb.append(" AND");
			}
			sb.append(" HEALTH_NOTE_ID = ").append(noteId);
		}
		if (id == null || before) sb.append(" ORDER BY ID DESC");
		sb.append(" LIMIT ").append(limit);
		sb.append(") AS T1 ORDER BY ID DESC");
		return (List<Diagnosis>) getSession().createSQLQuery(sb.toString()).addEntity(Diagnosis.class).list();
	}
}
