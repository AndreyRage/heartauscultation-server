package ua.org.horishniy.heartauscultation.server.controllers;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.org.horishniy.heartauscultation.server.entities.Experience;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;
import ua.org.horishniy.heartauscultation.server.exceptions.ForbidenException;
import ua.org.horishniy.heartauscultation.server.exceptions.NotFoundException;
import ua.org.horishniy.heartauscultation.server.service.ExperienceService;
import ua.org.horishniy.heartauscultation.server.service.UserService;
import ua.org.horishniy.heartauscultation.server.service.entities.requests.OkResponse;

import java.util.List;

/**
 * Created by rage on 04.04.16.
 */
@Controller
public class UserController {

	@Autowired
	private UserService mUserService;

	@Autowired
	private ExperienceService mExperienceService;

	@RequestMapping(value = {"api/users/my"}, method = RequestMethod.GET)
	public ResponseEntity<User> getMyUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return ResponseEntity.ok().body((User) auth.getPrincipal());
	}

	@RequestMapping(value = {"api/users/{id}"}, method = RequestMethod.GET)
	public ResponseEntity getUserById(@PathVariable("id") long id) {
		User user = mUserService.get(id);
		if (user == null) {
			throw new NotFoundException("User not found");
		}
		return ResponseEntity.ok().body(user);
	}

	@RequestMapping(value = {"api/users/my"}, method = RequestMethod.PUT)
	public ResponseEntity editMyUser(@RequestBody User user) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return editUser((User) auth.getPrincipal(), user);
	}

	@RequestMapping(value = {"api/users/my/password"}, method = RequestMethod.PUT)
	public ResponseEntity changePassword(@RequestHeader("currentPassword") String currentPassword,
										 @RequestHeader("password") String password,
										 @RequestHeader("rePassword") String rePassword) {
		mUserService.updatePassword(currentPassword, password, rePassword);
		return ResponseEntity.ok(new OkResponse("Password successfully updated"));
	}

	//@Secured("ADMIN")
	@RequestMapping(value = {"api/users/{id}"}, method = RequestMethod.PUT)
	public ResponseEntity editUserById(@PathVariable("id") long id, @RequestBody User user) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null || !auth.getAuthorities().contains(Role.ADMIN)) {
			throw new ForbidenException("Permission denied");
		}
		return editUser(mUserService.get(id), user);
	}

	private ResponseEntity editUser(@NotNull User existUser,@NotNull User user) {
		mUserService.updateUser(user, existUser);
		User updatedUser = mUserService.get(existUser.getId());
		if (updatedUser == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Server error");
		}
		return ResponseEntity.ok().body(updatedUser);
	}

	@RequestMapping(value = {"api/users/my/experiences"}, method = RequestMethod.GET)
	public ResponseEntity getMyUserExperience() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		return ResponseEntity.ok().body(mExperienceService.getExperienceByUserId(user.getId()));
	}

	@RequestMapping(value = {"api/users/{id}/experiences"}, method = RequestMethod.GET)
	public ResponseEntity getExperienceByUserId(@PathVariable("id") long id) {
		return ResponseEntity.ok().body(mExperienceService.getExperienceByUserId(id));
	}

	@RequestMapping(value = {"api/users/my/experiences"}, method = RequestMethod.PUT)
	public ResponseEntity editMyUserExperience(@RequestBody List<Experience> experience) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		return editExperience(user.getId(), experience);
	}

	@RequestMapping(value = {"api/users/{id}/experiences"}, method = RequestMethod.PUT)
	public ResponseEntity editUserExperience(@PathVariable("id") long id, @RequestBody List<Experience> experience) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null || !auth.getAuthorities().contains(Role.ADMIN)) {
			throw new ForbidenException("Permission denied");
		}
		return editExperience(id, experience);
	}

	public ResponseEntity editExperience(long userId, List<Experience> experience) {
		mExperienceService.addExperience(experience, userId);
		return ResponseEntity.ok().body(mExperienceService.getExperienceByUserId(userId));
	}

	@RequestMapping(value = {"api/users/my/experiences"}, method = RequestMethod.DELETE)
	public ResponseEntity deleteMyUserExperience(@RequestBody List<Long> experienceIds) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		return deleteExperience(user.getId(), experienceIds);
	}

	@RequestMapping(value = {"api/users/{id}/experiences"}, method = RequestMethod.DELETE)
	public ResponseEntity deleteUserExperience(@PathVariable("id") long id, @RequestBody List<Long> experienceIds) {
		return deleteExperience(id, experienceIds);
	}

	public ResponseEntity deleteExperience(long userId, List<Long> experienceIds) {
		mExperienceService.deleteExperience(experienceIds, userId);
		return ResponseEntity.ok().body(mExperienceService.getExperienceByUserId(userId));
	}

	@RequestMapping(value = {"api/users"}, method = RequestMethod.GET)
	public ResponseEntity findUsers(@RequestParam("search") String search,
									@RequestParam(value = "role", required = false) Role role) {
		return ResponseEntity.ok(mUserService.findUsers(search, role));
	}
}
