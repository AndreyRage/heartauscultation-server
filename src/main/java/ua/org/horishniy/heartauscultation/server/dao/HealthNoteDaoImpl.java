package ua.org.horishniy.heartauscultation.server.dao;

import org.springframework.stereotype.Repository;
import ua.org.horishniy.heartauscultation.server.entities.HealthNote;

import org.jetbrains.annotations.Nullable;
import ua.org.horishniy.heartauscultation.server.utils.TextUtils;

import java.util.List;

/**
 * Created by rage on 09.03.16.
 */
@Repository("healthNoteDao")
public class HealthNoteDaoImpl extends AbstractDao<Long, HealthNote> implements HealthNoteDao {
	private static final int LIMIT = 20;

	public HealthNoteDaoImpl() {
		super(HealthNote.class);
	}

	@Override
	public List<HealthNote> getBeforeId(@Nullable Long id, int limit) {
		return getBeforeId(id, null, limit);
	}

	@Override
	public List<HealthNote> getSinceId(@Nullable Long id, int limit) {
		return getSinceId(id, null, limit);
	}

	@Override
	public List<HealthNote> getBeforeId(@Nullable Long id, @Nullable Long userId, int limit) {
		return getBeforeId(id, null, userId, limit);
	}

	@Override
	public List<HealthNote> getSinceId(@Nullable Long id, @Nullable Long userId, int limit) {
		return getSinceId(id, null, userId, limit);
	}

	@Override
	public List<HealthNote> getBeforeId(@Nullable Long id, @Nullable String search, @Nullable Long userId, int limit) {
		return createSqlQuery(true, id, search, userId, limit);
	}

	@Override
	public List<HealthNote> getSinceId(@Nullable Long id, @Nullable String search, @Nullable Long userId, int limit) {
		return createSqlQuery(false, id, search, userId, limit);
	}

	@SuppressWarnings("unchecked")
	private List<HealthNote> createSqlQuery(boolean before, @Nullable Long id, String search, @Nullable Long userId, int limit) {
		if (limit == 0) {
			limit = LIMIT;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM (SELECT * FROM HEALTH_NOTE");
		if (id != null || search != null || userId != null) {
			sb.append(" WHERE");
		}
		if (!TextUtils.isEmpty(search)) {
			sb.append(" (");
			StringBuilder s = new StringBuilder();
			for (String w: search.split(" ")) {
				if (s.length() > 0) {
					s.append(" AND ");
				}
				s.append("DESCRIPTION LIKE '%");
				s.append(w).append("%'");
			}
			sb.append(s).append(")");
		}
		if (id != null) {
			if (search != null) {
				sb.append(" AND");
			}
			sb.append(" ID");
			sb.append(before ? " < " : " > ");
			sb.append(id);
		}
		if (userId != null) {
			if (id != null || search != null) {
				sb.append(" AND");
			}
			sb.append(" USER_ID = ").append(userId);
		}
		if (id == null || before) sb.append(" ORDER BY ID DESC");
		sb.append(" LIMIT ").append(limit);
		sb.append(") AS T1 ORDER BY ID DESC");
		return (List<HealthNote>) getSession().createSQLQuery(sb.toString()).addEntity(HealthNote.class).list();
	}
}
