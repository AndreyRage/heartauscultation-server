package ua.org.horishniy.heartauscultation.server.enums;

/**
 * Created by rage on 09.03.16.
 */
public enum Measure {
    PCS,
    KILO_G, G, MILLI_G, MICRO_G, NANO_G,
    KILO_L, L, MILLI_L, MICRO_L, NANO_L
}
