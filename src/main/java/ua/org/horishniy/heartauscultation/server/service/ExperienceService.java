package ua.org.horishniy.heartauscultation.server.service;

import ua.org.horishniy.heartauscultation.server.entities.Experience;

import java.util.List;

/**
 * Created by rage on 10.03.16.
 */
public interface ExperienceService extends BaseService<Long, Experience> {
	List<Experience> getExperienceByUserId(Long userId);
	void addExperience(List<Experience> experience, Long userId);
	void deleteExperience(List<Long> listId, Long userId);
}
