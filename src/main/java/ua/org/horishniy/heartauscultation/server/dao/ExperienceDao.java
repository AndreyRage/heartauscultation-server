package ua.org.horishniy.heartauscultation.server.dao;

import ua.org.horishniy.heartauscultation.server.entities.Experience;

import java.util.List;

/**
 * Created by rage on 10.03.16.
 */
public interface ExperienceDao extends BaseDao<Long, Experience> {
	List<Experience> getExperienceByUserId(Long userId);
	void addExperience(List<Experience> experience);
	void deleteExperience(List<Long> listId);
}
