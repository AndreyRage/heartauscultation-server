package ua.org.horishniy.heartauscultation.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.org.horishniy.heartauscultation.server.dao.DrugDao;
import ua.org.horishniy.heartauscultation.server.entities.Drug;
import ua.org.horishniy.heartauscultation.server.utils.TextUtils;

/**
 * Created by rage on 12.03.16.
 */
@Service("drugService")
@Transactional
public class DrugServiceImpl extends AbstractService<Long, Drug> implements DrugService {

	@Autowired
	DrugServiceImpl(DrugDao dao) {
		super(dao);
	}

	@Override
	public DrugDao getDao() {
		return (DrugDao) super.getDao();
	}

	@Override
	public boolean isValid(Drug drug) {
		if (drug != null && drug.getHealthNote() != null
				&& !TextUtils.isEmpty(drug.getName())) {
			return true;
		}
		return false;
	}
}
