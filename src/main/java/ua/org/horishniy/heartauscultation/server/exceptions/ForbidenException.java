package ua.org.horishniy.heartauscultation.server.exceptions;

/**
 * Created by rage on 05.04.16.
 */
public class ForbidenException extends RuntimeException {
	public ForbidenException(String message) {
		super(message);
	}
}
