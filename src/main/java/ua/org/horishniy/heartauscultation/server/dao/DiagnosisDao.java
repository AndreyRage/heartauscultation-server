package ua.org.horishniy.heartauscultation.server.dao;

import org.jetbrains.annotations.Nullable;
import ua.org.horishniy.heartauscultation.server.entities.Diagnosis;

import java.util.List;

/**
 * Created by rage on 09.03.16.
 */
public interface DiagnosisDao extends BaseDao<Long, Diagnosis> {
	List<Diagnosis> getBeforeId(@Nullable Long id, int limit);
	List<Diagnosis> getSinceId(@Nullable Long id, int limit);
	List<Diagnosis> getBeforeId(@Nullable Long id, @Nullable Long noteId, int limit);
	List<Diagnosis> getSinceId(@Nullable Long id, @Nullable Long noteId, int limit);
}
