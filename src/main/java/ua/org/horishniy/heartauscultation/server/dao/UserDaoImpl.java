package ua.org.horishniy.heartauscultation.server.dao;

import org.hibernate.SQLQuery;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;
import ua.org.horishniy.heartauscultation.server.utils.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rage on 09.03.16.
 */
@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Long, User> implements UserDao {

	public UserDaoImpl() {
		super(User.class);
	}

	@Nullable
	@Override
	public User getByName(String name) {
		SQLQuery query = getSession().createSQLQuery("SELECT * FROM USER WHERE NAME = :name");
		query.setString("name", name);
		List list = query.addEntity(User.class).list();
		if (list != null && !list.isEmpty()) {
			return (User) list.get(0);
		}
		return null;
	}

	@Override
	public boolean isUserNameAlreadyExists(String name) {
		SQLQuery query = getSession().createSQLQuery("SELECT COUNT(*) FROM USER WHERE NAME = :name");
		query.setString("name", name);
		Number count = (Number) query.uniqueResult();
		return count.intValue() > 0;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<User> findUsers(String search, Role role) {
		if (TextUtils.isEmpty(search)) {
			return new ArrayList<>();
		}
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM USER WHERE (");
		int count  = 0;
		for (String w: search.split(" ")) {
			count++;
			if (count != 1) {
				sb.append(" OR");
			}
			sb.append(" NAME LIKE '%").append(w).append("%' OR");
			sb.append(" FIRST_NAME LIKE '%").append(w).append("%' OR");
			sb.append(" LAST_NAME LIKE '%").append(w).append("%'");
		}
		sb.append(")");
		if (role != null) {
			sb.append(" AND ROLE = ").append(role.ordinal());
		}
		sb.append(" LIMIT 10");
		return (List<User>) getSession().createSQLQuery(sb.toString()).addEntity(User.class).list();
	}
}
