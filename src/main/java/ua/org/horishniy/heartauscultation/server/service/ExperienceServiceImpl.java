package ua.org.horishniy.heartauscultation.server.service;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.org.horishniy.heartauscultation.server.dao.ExperienceDao;
import ua.org.horishniy.heartauscultation.server.entities.Experience;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;
import ua.org.horishniy.heartauscultation.server.exceptions.BadRequestException;
import ua.org.horishniy.heartauscultation.server.utils.TextUtils;

import java.util.List;

/**
 * Created by rage on 12.03.16.
 */
@Service("experienceService")
@Transactional
public class ExperienceServiceImpl extends AbstractService<Long, Experience> implements ExperienceService {

	@Autowired
	public UserService mUserService;

	@Autowired
	ExperienceServiceImpl(ExperienceDao dao) {
		super(dao);
	}

	@Override
	public ExperienceDao getDao() {
		return (ExperienceDao) super.getDao();
	}

	@Override
	public List<Experience> getExperienceByUserId(Long userId) {
		return getDao().getExperienceByUserId(userId);
	}

	@Override
	public void addExperience(List<Experience> experience, Long userId) {
		checkPermission(userId);
		List<Experience> currentExperience = getExperienceByUserId(userId);
		experience.forEach(exp -> {
			isValid(exp);
			exp.setUserId(userId);
			if (exp.getId() != null) {
				final boolean[] isNotThisUserExperience = {true};
				if (currentExperience != null && !currentExperience.isEmpty()) {
					currentExperience.forEach(currentExp -> {
						if (exp.getId().equals(currentExp.getId())) {
							isNotThisUserExperience[0] = false;
						}
					});
				}
				if (isNotThisUserExperience[0]) {
					throw new BadRequestException("Bad experience id. This experience owned by another user.");
				}
			}
		});
		getDao().addExperience(experience);
	}

	@Override
	public void deleteExperience(List<Long> listId, Long userId) {
		checkPermission(userId);
		getDao().deleteExperience(listId);
	}

	@Override
	public boolean isValid(Experience experience) {
		if (experience == null) {
			throw new BadRequestException("Experience is empty");
		}
		if (experience.getName() != null) {
			experience.setName(experience.getName().trim());
		}
		if (TextUtils.isEmpty(experience.getName())) {
			throw new BadRequestException("Experience name is empty");
		}
		if (experience.getFromYear() > 0 && experience.getToYear() > 0) {
			LocalDate start = new LocalDate(experience.getFromYear(), experience.getFromMonth() > 0 ? experience.getFromMonth() : 1, experience.getFromDay() > 0 ? experience.getFromDay() : 1);
			LocalDate end = new LocalDate(experience.getToYear(), experience.getToMonth() > 0 ? experience.getToMonth() : 1, experience.getToDay() > 0 ? experience.getToDay() : 1);
			if (start.isAfter(end)) {
				throw new BadRequestException("Start date is after the end date");
			}
		}
		return true;
	}

	private void checkPermission(Long userId) {
		User user = mUserService.get(userId);
		if (user == null) {
			throw new BadRequestException("Wrong user id. User not exists.");
		}
		if (!Role.DOCTOR.equals(user.getRole())) {
			throw new BadRequestException("This user can't have experience");
		}
	}
}
