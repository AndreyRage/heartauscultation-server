/**
 * Created by rage on 31.03.16.
 */

$(function () {
    navSignIn();
    $("#nav-login").click(navSignIn);
    $("#nav-registration").click(navSignUp);
    $("form.form-signin").submit(function (e) {
        e.preventDefault();
        doLogin();
    });
    $("form.form-signup").submit(function (e) {
        e.preventDefault();
        doRegister();
    });

    var timer;
    $("#register-username").keyup(function () {
        clearTimeout(timer);
        var val = this.value;
        if (val.length >= 3) {
            timer = setTimeout(function () {
                doCheckUsername(val);
            }, 400);
        } else if (val.length > 20) {
            showRegisterError("Username is not valid (maximum 20 characters)");
        } else if (val.length < 3) {
            showRegisterError(null);
        }
    });
});

function navSignIn() {
    $("#login").show();
    $("#registration").hide();
    $("#nav-login").addClass('active');
    $("#nav-registration").removeClass('active');
}

function navSignUp() {
    $("#login").hide();
    $("#registration").show();
    $("#nav-login").removeClass('active');
    $("#nav-registration").addClass('active');
}

function showLoginError(message) {
    if (message == null) {
        $("#login-alert").html('');
    } else {
        $("#login-alert").html('<div id="invalid-credentials" class="alert alert-danger"><p>' + message + '</p></div>');
    }
}

function showRegisterError(message) {
    if (message == null) {
        $("#register-alert").html('');
    } else {
        $("#register-alert").html('<div id="invalid-credentials" class="alert alert-danger"><p>' + message + '</p></div>');
    }
}

function doLogin() {
    var username = $("#login-username").val();
    var password = $("#login-password").val();

    $.ajax({
        url: '/api/login',
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ':' + password));
        },
        success: function () {
            window.location.replace("/");
        },
        error: function (xhr, status, error) {
            if (xhr.status == 401) {
                showLoginError("The username or password is incorrect.");
            } else {
                showLoginError(handleError(xhr, status));
            }
        }
    });
}

function doCheckUsername(username) {
    $.ajax({
        url: '/api/check-username?username=' + username,
        type: "GET",
        success: function (data, status, xhr) {
            try {
                var obj = jQuery.parseJSON(xhr.responseText);
                if (obj.isUsernameExists) {
                    showRegisterError("Username already exists");
                } else {
                    showRegisterError(null);
                }
            } catch (e) {
            }
        }
    });
}

function doRegister() {
    var fUsername = $("#register-username");
    var fEmail = $("#register-email");
    var fRole = $("#register-role");
    var fPassword = $("#register-password");
    var fRePassword = $("#register-re-password");

    var username = fUsername.val();
    var email = fEmail.val();
    var role = fRole.val();
    var password = fPassword.val();
    var rePassword = fRePassword.val();

    if (username.length < 3) {
        showRegisterError("Username is not valid (minimum is 3 characters)");
        fUsername.focus();
        scrollTop("#registration h2");
        return;
    }
    if (username.length > 20) {
        showRegisterError("Username is not valid (maximum 20 characters)");
        fUsername.focus();
        scrollTop("#registration h2");
        return;
    }
    if (!isEmail(email)) {
        showRegisterError("Email is not valid");
        fEmail.focus();
        scrollTop("#registration h2");
        return;
    }
    if (password.length < 6) {
        showRegisterError("Password is too short (minimum is 6 characters)");
        fPassword.focus();
        scrollTop("#registration h2");
        return;
    }
    if (password != rePassword) {
        showRegisterError("Password not match");
        fPassword.focus();
        scrollTop("#registration h2");
        return;
    }

    $.ajax({
        url: '/api/register',
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('name', username);
            xhr.setRequestHeader('email', email);
            xhr.setRequestHeader('role', role);
            xhr.setRequestHeader('password', password);
            xhr.setRequestHeader('rePassword', rePassword);
        },
        success: function () {
            window.location.replace("/?register");
        },
        error: function (xhr, status, error) {
            showRegisterError(handleError(xhr, status));
        }
    });
}