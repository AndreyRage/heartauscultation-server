<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 26.03.16
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/navbar.jsp"/>
<div class="container">
    <div class="center-content-style">
        <div class="container-body">
            <form class="form-add-note form-style">
                <div id="note-alert"></div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Record:</div>
                        <div id="file-content" class="col-sm-6 no-padding">
                            <c:if test="${note.getRecFile() != null}">
                                <audio controls>
                                    <c:url var="contentUrl" value="/content"/>
                                    <source src="${contentUrl}/${note.getRecFile()}">
                                    Your browser does not support the audio element.
                                </audio>
                            </c:if>
                            <c:if test="${note.getRecFile() == null}">
                                <label for="diastolic" class="sr-only">File</label>
                                <input type="file" id="file" name="file" placeholder="File" value="">
                            </c:if>
                        </div>
                        <div class="col-sm-2 no-padding">
                            <a id="btn-delete-file" class="btn btn-default drug-group-delete">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Blood pressure:</div>
                        <div class="col-sm-4 no-padding">
                            <label for="systolic" class="sr-only">Systolic</label>
                            <input type="number" id="systolic" class="form-control" name="systolic" placeholder="Systolic" value="${note.getSystolic()}" autofocus>
                        </div>
                        <div class="col-sm-4 no-padding">
                            <label for="diastolic" class="sr-only">Diastolic</label>
                            <input type="number" id="diastolic" class="form-control" name="diastolic" placeholder="Diastolic" value="${note.getDiastolic()}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Drugs:</div>
                        <div class="col-sm-8 no-padding">
                            <div id="drug-group">
                                <c:if test="${not empty note.getDrugs()}">
                                    <c:forEach var="drug" items="${note.getDrugs()}">
                                        <div class="row no-padding drug-group-item">
                                            <div class="col-sm-6 no-padding">
                                                <input type="text" class="form-control" name="drug-name" placeholder="Name" value="${drug.getName()}">
                                            </div>
                                            <div class="col-sm-2 no-padding">
                                                <input type="number" step="0.01" class="form-control" name="drug-dosage" placeholder="Dosage" value="${drug.getDosage()}">
                                            </div>
                                            <div class="col-sm-2 no-padding">
                                                <select class="form-control center-field form-select" name="drug-measure" title="Dosage">
                                                    <option></option>
                                                    <option value="PCS" ${drug.getMeasure() == 'PCS' ? 'selected="selected"' : '' }>pcs</option>
                                                    <option value="KILO_G" ${drug.getMeasure() == 'KILO_G' ? 'selected="selected"' : '' }>kg</option>
                                                    <option value="G" ${drug.getMeasure() == 'G' ? 'selected="selected"' : '' }>g</option>
                                                    <option value="MILLI_G" ${drug.getMeasure() == 'MILLI_G' ? 'selected="selected"' : '' }>mg</option>
                                                    <option value="MICRO_G" ${drug.getMeasure() == 'MICRO_G' ? 'selected="selected"' : '' }>µg</option>
                                                    <option value="NANO_G" ${drug.getMeasure() == 'NANO_G' ? 'selected="selected"' : '' }>ng</option>
                                                    <option value="L" ${drug.getMeasure() == 'L' ? 'selected="selected"' : '' }>l</option>
                                                    <option value="MILLI_L" ${drug.getMeasure() == 'MILLI_L' ? 'selected="selected"' : '' }>ml</option>
                                                    <option value="MICRO_L" ${drug.getMeasure() == 'MICRO_L' ? 'selected="selected"' : '' }>µl</option>
                                                    <option value="NANO_L" ${drug.getMeasure() == 'NANO_L' ? 'selected="selected"' : '' }>nl</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2 no-padding">
                                                <a class="btn btn-default drug-group-delete">Delete</a>
                                            </div>
                                            <input type="hidden" class="drug-id" name="drug-id" value="${drug.getId()}"/>
                                        </div>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${note.getDrugs() == null || note.getDrugs().size() < 10}">
                                    <div class="row no-padding drug-group-item">
                                        <div class="col-sm-6 no-padding">
                                            <input type="text" class="form-control" name="drug-name" placeholder="Name">
                                        </div>
                                        <div class="col-sm-2 no-padding">
                                            <input type="number" step="0.01" class="form-control" name="drug-dosage" placeholder="Dosage">
                                        </div>
                                        <div class="col-sm-2 no-padding">
                                            <select class="form-control center-field form-select" name="drug-measure" title="Dosage">
                                                <option></option>
                                                <option value="PCS">pcs</option>
                                                <option value="KILO_G">kg</option>
                                                <option value="G">g</option>
                                                <option value="MILLI_G">mg</option>
                                                <option value="MICRO_G">µg</option>
                                                <option value="NANO_G">ng</option>
                                                <option value="L">l</option>
                                                <option value="MILLI_L">ml</option>
                                                <option value="MICRO_L">µl</option>
                                                <option value="NANO_L">nl</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2 no-padding">
                                            <a class="btn btn-default drug-group-delete">Delete</a>
                                        </div>
                                        <input type="hidden" class="drug-id" name="drug-id" value=""/>
                                    </div>
                                </c:if>
                            </div>
                            <div class="row no-padding">
                                <div class="col-sm-12 no-padding">
                                    <a id="drug-group-button-add" class="btn btn-default" href="#">Add drug</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Activity:</div>
                        <div class="col-sm-8 no-padding">
                            <label class="radio-inline"><input type="radio" name="activity" value="REST" ${note.getAction() == 'REST' ? 'checked="checked"' : '' }>Rest</label>
                            <label class="radio-inline"><input type="radio" name="activity" value="WARM_UP" ${note.getAction() == 'WARM_UP' ? 'checked="checked"' : '' }>Warm up</label>
                            <label class="radio-inline"><input type="radio" name="activity" value="RUN" ${note.getAction() == 'RUN' ? 'checked="checked"' : '' }>Run</label>
                            <label class="radio-inline"><input type="radio" name="activity" value="EXTREME" ${note.getAction() == 'EXTREME' ? 'checked="checked"' : '' }>Extreme</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding">Description:</div>
                        <div class="col-sm-8 no-padding">
                            <label for="description" class="sr-only">Diastolic</label>
                            <textarea id="description" class="form-control" rows="5" name="description">${note.getDescription()}</textarea>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="id" name="id" value="${note.getId()}"/>
                <input type="hidden" id="current-file" name="id" value="${note.getRecFile()}"/>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6 no-padding"></div>
                        <div class="col-sm-3">
                            <c:if test="${note != null}">
                                <button id="btn-delete-note" class="btn btn-lg btn-danger btn-block">Delete note</button>
                            </c:if>
                        </div>
                        <div class="col-sm-3">
                            <c:if test="${note == null}">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Add note</button>
                            </c:if>
                            <c:if test="${note != null}">
                                <button class="btn btn-lg btn-success btn-block" type="submit">Edit note</button>
                            </c:if>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<c:url value='/static/jquery/jquery-1.12.2.min.js' />"></script>
<script>window.jQuery || document.write('<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"><\/script>')</script>
<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"></script>
<script src="<c:url value='/static/js/health-note-edit.js' />"></script>
<script src="<c:url value='/static/js/utils.js' />"></script>

<jsp:include page="include/footer.jsp"/>
