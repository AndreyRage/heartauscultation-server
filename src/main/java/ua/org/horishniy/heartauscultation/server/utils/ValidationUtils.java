package ua.org.horishniy.heartauscultation.server.utils;

import org.apache.commons.validator.routines.EmailValidator;
import org.jetbrains.annotations.Nullable;

/**
 * Created by rage on 12.03.16.
 */
public class ValidationUtils {

	public static boolean isUserNameValid(@Nullable String name) {
		return name != null && (name.length() >= 3 && name.length() <= 20);
	}

	public static boolean isPasswordValid(@Nullable String password) {
		return password != null && password.length() >= 6;
	}

	public static boolean isEmailValid(@Nullable String email) {
		return EmailValidator.getInstance().isValid(email);
	}
}
