package ua.org.horishniy.heartauscultation.server.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.org.horishniy.heartauscultation.server.configuration.AppInitializer;
import ua.org.horishniy.heartauscultation.server.exceptions.NotFoundException;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;

@Controller
public class FileDownloadController {

    @RequestMapping(value="/content/{file:.+}", method = RequestMethod.GET)
    public void downloadFile(HttpServletResponse response, @PathVariable("file") String fileName) throws IOException {
        File uploadRootDir = new File(AppInitializer.FILE_LOCATION);
        File file = new File(uploadRootDir.getAbsolutePath() + File.separator + fileName);
         
        if(!file.exists()){
            throw new NotFoundException("Sorry. The file you are looking for does not exist");
        }
         
        String mimeType= URLConnection.guessContentTypeFromName(file.getName());
        if(mimeType==null){
            mimeType = "application/octet-stream";
        }

        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", String.format("inline; filename=\"%s\"", file.getName()));
        response.setContentLength((int)file.length());
 
        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }
}