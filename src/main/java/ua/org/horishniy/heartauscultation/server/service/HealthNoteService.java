package ua.org.horishniy.heartauscultation.server.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.multipart.MultipartFile;
import ua.org.horishniy.heartauscultation.server.entities.HealthNote;

import java.util.List;

/**
 * Created by rage on 12.03.16.
 */
public interface HealthNoteService extends BaseService<Long, HealthNote> {
	Long add(HealthNote healthNote, @Nullable MultipartFile file);
	void merge(HealthNote healthNote);
	List<HealthNote> getBeforeId(@Nullable Long id, int limit);
	List<HealthNote> getSinceId(@Nullable Long id, int limit);
	List<HealthNote> getBeforeId(@Nullable Long id, @Nullable Long userId, int limit);
	List<HealthNote> getSinceId(@Nullable Long id, @Nullable Long userId, int limit);
	List<HealthNote> getBeforeId(@Nullable Long id, @Nullable String search, @Nullable Long userId, int limit);
	List<HealthNote> getSinceId(@Nullable Long id, @Nullable String search, @Nullable Long userId, int limit);
	List<HealthNote> getHealthNotes(boolean before, @Nullable Long id, @Nullable String search, @Nullable Long userId, int limit);
	List<HealthNote> getHealthNotes(@Nullable Long sinceId, @Nullable Long beforeId, @Nullable String search, @Nullable Long userId, @Nullable Integer limit);
}
