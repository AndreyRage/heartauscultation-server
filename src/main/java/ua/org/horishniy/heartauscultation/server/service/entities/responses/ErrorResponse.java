package ua.org.horishniy.heartauscultation.server.service.entities.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rage on 03.04.16.
 */
public class ErrorResponse {
	@JsonProperty("code")
	private Integer mCode;

	@JsonProperty("message")
	private String mMessage;

	public ErrorResponse() {
	}

	public ErrorResponse(String message) {
		mMessage = message;
	}

	public ErrorResponse(Integer code, String message) {
		mCode = code;
		mMessage = message;
	}

	public Integer getCode() {
		return mCode;
	}

	public void setCode(Integer code) {
		mCode = code;
	}

	public String getMessage() {
		return mMessage;
	}

	public void setMessage(String message) {
		mMessage = message;
	}
}
