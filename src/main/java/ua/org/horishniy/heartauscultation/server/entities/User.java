package ua.org.horishniy.heartauscultation.server.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ua.org.horishniy.heartauscultation.server.enums.Role;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by rage on 09.03.16.
 */
@Entity
@Table(name = "USER")
public class User extends AbstractEntity implements UserDetails {

	@Column(name = "NAME", length = 20, nullable = false)
	private String mName;

	@Column(name = "PASSWORD", length = 60, nullable = false)
	@JsonIgnore
	private String mPassword;

	@Column(name = "EMAIL", length = 255, nullable = false)
	private String mEmail;

	@Column(name = "ROLE", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private Role mRole;

	@Column(name = "FIRST_NAME")
	@JsonProperty("firstName")
	private String mFirstName;

	@Column(name = "LAST_NAME")
	@JsonProperty("lastName")
	private String mLastName;

	@Column(name="BIRTHDAY")
	@JsonProperty("birthday")
	//@JsonIgnore
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	public LocalDate mBirthday;

	//DOCTOR DATA
	//@OneToMany(mappedBy = "mUser", fetch = FetchType.EAGER)
	//private Set<Experience> mExperiences;

	/*@OneToMany(mappedBy = "mDoctorUser", fetch = FetchType.EAGER)
	private Set<Diagnosis> mDiagnosises;

	//PATIENT DATA
	@OneToMany(mappedBy = "mUser", fetch = FetchType.EAGER)
	private Set<HealthNote> mHealthNotes;*/

	public User() {
	}

	public User(String name, String password, String email, Role role) {
		mName = name;
		mPassword = password;
		mEmail = email;
		mRole = role;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		User user = (User) o;

		if (mName != null ? !mName.equals(user.mName) : user.mName != null) return false;
		if (mPassword != null ? !mPassword.equals(user.mPassword) : user.mPassword != null) return false;
		if (mEmail != null ? !mEmail.equals(user.mEmail) : user.mEmail != null) return false;
		if (mRole != user.mRole) return false;
		if (mFirstName != null ? !mFirstName.equals(user.mFirstName) : user.mFirstName != null) return false;
		if (mLastName != null ? !mLastName.equals(user.mLastName) : user.mLastName != null) return false;
		return mBirthday != null ? mBirthday.equals(user.mBirthday) : user.mBirthday == null;

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (mName != null ? mName.hashCode() : 0);
		result = 31 * result + (mPassword != null ? mPassword.hashCode() : 0);
		result = 31 * result + (mEmail != null ? mEmail.hashCode() : 0);
		result = 31 * result + (mRole != null ? mRole.hashCode() : 0);
		result = 31 * result + (mFirstName != null ? mFirstName.hashCode() : 0);
		result = 31 * result + (mLastName != null ? mLastName.hashCode() : 0);
		result = 31 * result + (mBirthday != null ? mBirthday.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "User{" +
				"mName='" + mName + '\'' +
				", mPassword='" + mPassword + '\'' +
				", mEmail='" + mEmail + '\'' +
				", mRole=" + mRole +
				", mFirstName='" + mFirstName + '\'' +
				", mLastName='" + mLastName + '\'' +
				", mBirthday=" + mBirthday +
				'}';
	}

	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.singletonList(getRole());
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return true;
	}

	@Override
	@JsonIgnoreProperties("username")
	public String getUsername() {
		return mName;
	}

	public void setUsername(String name) {
		mName = name;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return mPassword;
	}

	public void setPassword(String password) {
		mPassword = password;
	}

	public String getEmail() {
		return mEmail;
	}

	public void setEmail(String email) {
		mEmail = email;
	}

	@JsonIgnoreProperties("role")
	public Role getRole() {
		return mRole;
	}

	public void setRole(Role role) {
		mRole = role;
	}

	public String getFirstName() {
		return mFirstName;
	}

	public void setFirstName(String firstName) {
		mFirstName = firstName;
	}

	public String getLastName() {
		return mLastName;
	}

	public void setLastName(String lastName) {
		mLastName = lastName;
	}

	public LocalDate getBirthday() {
		return mBirthday;
	}

	public void setBirthday(LocalDate birthday) {
		mBirthday = birthday;
	}

	/*public Set<Experience> getExperiences() {
		return mExperiences;
	}

	public void setExperiences(Set<Experience> experiences) {
		mExperiences = experiences;
	}*/

	public String showUsername() {
		return getFirstName() != null ? getFirstName() + (getLastName() != null ? " " + getLastName() : "") : getUsername();
	}
}
