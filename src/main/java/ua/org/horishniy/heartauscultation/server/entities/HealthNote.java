package ua.org.horishniy.heartauscultation.server.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import ua.org.horishniy.heartauscultation.server.enums.Action;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by rage on 08.03.16.
 */
@Entity
@Table(name = "HEALTH_NOTE")
public class HealthNote extends AbstractEntity {

    /*@ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User mUser;*/

    @Column(name = "USER_ID")
    @JsonProperty("userId")
    private Long mUserId;

    @Column(name = "DEVICE")
    @JsonProperty("device")
    private String mDevice;

    @Column(name = "REC_FILE")
    @JsonProperty("recFile")
    private String mRecFile;

    @Column(name = "SYSTOLIC")
    @JsonProperty("systolic")
    private Short mSystolic;

    @Column(name = "DIASTOLIC")
    @JsonProperty("diastolic")
    private Short mDiastolic;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            mappedBy = "mHealthNote",
            orphanRemoval=true)
    @JsonProperty("drugs")
    private Set<Drug> mDrugs;

    @Column(name = "ACTION")
    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("action")
    private Action mAction;

    @Column(name = "DESCRIPTION")
    @JsonProperty("description")
    private String mDescription;

    /*@OneToMany(mappedBy = "mHealthNote", fetch = FetchType.LAZY)
    private Set<Diagnosis> mDiagnosises;*/

    public HealthNote() {
    }

    public HealthNote(Long userId, String device, String recFile, Short systolic, Short diastolic, Set<Drug> drugs, Action action, String description) {
        mUserId = userId;
        mDevice = device;
        mRecFile = recFile;
        mSystolic = systolic;
        mDiastolic = diastolic;
        mDrugs = drugs;
        mAction = action;
        mDescription = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        HealthNote that = (HealthNote) o;

        if (mUserId != null ? !mUserId.equals(that.mUserId) : that.mUserId != null) return false;
        if (mDevice != null ? !mDevice.equals(that.mDevice) : that.mDevice != null) return false;
        if (mRecFile != null ? !mRecFile.equals(that.mRecFile) : that.mRecFile != null) return false;
        if (mSystolic != null ? !mSystolic.equals(that.mSystolic) : that.mSystolic != null) return false;
        if (mDiastolic != null ? !mDiastolic.equals(that.mDiastolic) : that.mDiastolic != null) return false;
        if (mDrugs != null ? !mDrugs.equals(that.mDrugs) : that.mDrugs != null) return false;
        if (mAction != that.mAction) return false;
        return mDescription != null ? mDescription.equals(that.mDescription) : that.mDescription == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (mUserId != null ? mUserId.hashCode() : 0);
        result = 31 * result + (mDevice != null ? mDevice.hashCode() : 0);
        result = 31 * result + (mRecFile != null ? mRecFile.hashCode() : 0);
        result = 31 * result + (mSystolic != null ? mSystolic.hashCode() : 0);
        result = 31 * result + (mDiastolic != null ? mDiastolic.hashCode() : 0);
        result = 31 * result + (mDrugs != null ? mDrugs.hashCode() : 0);
        result = 31 * result + (mAction != null ? mAction.hashCode() : 0);
        result = 31 * result + (mDescription != null ? mDescription.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HealthNote{" +
                "super=" + super.toString() +
                ", mUserId=" + mUserId +
                ", mDevice='" + mDevice + '\'' +
                ", mRecFile='" + mRecFile + '\'' +
                ", mSystolic=" + mSystolic +
                ", mDiastolic=" + mDiastolic +
                ", mDrugs=" + mDrugs +
                ", mAction=" + mAction +
                ", mDescription='" + mDescription + '\'' +
                '}';
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long userId) {
        mUserId = userId;
    }

    public String getDevice() {
        return mDevice;
    }

    public void setDevice(String device) {
        mDevice = device;
    }

    public String getRecFile() {
        return mRecFile;
    }

    public void setRecFile(String recFile) {
        mRecFile = recFile;
    }

    public Short getSystolic() {
        return mSystolic;
    }

    public void setSystolic(Short systolic) {
        mSystolic = systolic;
    }

    public Short getDiastolic() {
        return mDiastolic;
    }

    public void setDiastolic(Short diastolic) {
        mDiastolic = diastolic;
    }

    public Set<Drug> getDrugs() {
        return mDrugs;
    }

    public void setDrugs(Set<Drug> drugs) {
        mDrugs = drugs;
    }

    public Action getAction() {
        return mAction;
    }

    public void setAction(Action action) {
        mAction = action;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }
}
