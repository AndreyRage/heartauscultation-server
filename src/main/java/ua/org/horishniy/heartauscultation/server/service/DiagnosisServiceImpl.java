package ua.org.horishniy.heartauscultation.server.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.org.horishniy.heartauscultation.server.dao.DiagnosisDao;
import ua.org.horishniy.heartauscultation.server.entities.Diagnosis;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;
import ua.org.horishniy.heartauscultation.server.exceptions.BadRequestException;
import ua.org.horishniy.heartauscultation.server.exceptions.ForbidenException;
import ua.org.horishniy.heartauscultation.server.exceptions.NotFoundException;
import ua.org.horishniy.heartauscultation.server.utils.TextUtils;

import java.util.List;

/**
 * Created by rage on 12.03.16.
 */
@Service("diagnosisService")
@Transactional
public class DiagnosisServiceImpl extends AbstractService<Long, Diagnosis> implements DiagnosisService {

	@Autowired
	public UserService mUserService;

	@Autowired
	DiagnosisServiceImpl(DiagnosisDao dao) {
		super(dao);
	}

	@Override
	public DiagnosisDao getDao() {
		return (DiagnosisDao) super.getDao();
	}

	@Override
	public Long add(Diagnosis diagnosis) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if (!user.getAuthorities().contains(Role.DOCTOR)) {
			throw new ForbidenException("This user can't create diagnosis");
		}
		diagnosis.setDoctorUser(user);
		diagnosis.setDoctorUserId(user.getId());
		return super.add(diagnosis);
	}

	@Override
	public void update(Diagnosis diagnosis) {
		Diagnosis currentDiagnosis = get(diagnosis.getId());
		if (currentDiagnosis == null) {
			throw new NotFoundException("Diagnosis not found");
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if (!user.getId().equals(currentDiagnosis.getDoctorUserId())) {
			throw new BadRequestException("You can't edit not you diagnosis");
		}
		diagnosis.setDoctorUser(user);
		currentDiagnosis.setDescription(diagnosis.getDescription());
		currentDiagnosis.setMenace(diagnosis.getMenace());
		super.update(currentDiagnosis);
	}

	@Override
	public void delete(Long id) {
		Diagnosis diagnosis = get(id);
		if (diagnosis == null) {
			throw new NotFoundException("Diagnosis not found");
		}
		delete(diagnosis);
	}

	@Override
	public void delete(Diagnosis diagnosis) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if (!(user.getAuthorities().contains(Role.DOCTOR) || !user.getAuthorities().contains(Role.ADMIN))) {
			throw new ForbidenException("You can't delete diagnosis");
		}
		if (!diagnosis.getDoctorUserId().equals(user.getId())) {
			throw new ForbidenException("You can't delete diagnosis for another user");
		}
		super.delete(diagnosis);
	}

	@Override
	public List<Diagnosis> getBeforeId(@Nullable Long id, int limit) {
		List<Diagnosis> diagnosisList = getDao().getBeforeId(id, limit);
		loadUsers(diagnosisList);
		return diagnosisList;
	}

	@Override
	public List<Diagnosis> getSinceId(@Nullable Long id, int limit) {
		List<Diagnosis> diagnosisList = getDao().getSinceId(id, limit);
		loadUsers(diagnosisList);
		return diagnosisList;
	}

	@Override
	public List<Diagnosis> getBeforeId(@Nullable Long id, @Nullable Long noteId, int limit) {
		List<Diagnosis> diagnosisList = getDao().getBeforeId(id, noteId, limit);
		loadUsers(diagnosisList);
		return diagnosisList;
	}

	@Override
	public List<Diagnosis> getSinceId(@Nullable Long id, @Nullable Long noteId, int limit) {
		List<Diagnosis> diagnosisList = getDao().getSinceId(id, noteId, limit);
		loadUsers(diagnosisList);
		return diagnosisList;
	}

	@Override
	public List<Diagnosis> getDiagnosis(boolean before, @Nullable Long id, @Nullable Long noteId, int limit) {
		if (before)
			return getBeforeId(id, noteId, limit);
		else
			return getSinceId(id, noteId, limit);
	}

	@Override
	public List<Diagnosis> getDiagnosis(@Nullable Long sinceId, @Nullable Long beforeId, @Nullable Long noteId, @Nullable Integer limit) {
		if (sinceId != null && beforeId != null) {
			throw new BadRequestException("Can't use since and before ids together");
		}
		boolean before = false;
		Long id = null;
		if (sinceId != null) {
			id = sinceId;
		}
		if (beforeId != null) {
			id = beforeId;
			before = true;
		}
		if (limit == null) {
			limit = 0;
		}
		return getDiagnosis(before, id, noteId, limit);
	}

	private void loadUsers(List<Diagnosis> diagnosisList) {
		if (diagnosisList != null) {
			diagnosisList.forEach(diagnosis -> diagnosis.setDoctorUser(mUserService.get(diagnosis.getDoctorUserId())));
		}
	}

	@Override
	public boolean isValid(Diagnosis diagnosis) {
		if (diagnosis == null) {
			throw new BadRequestException("Diagnosis is null");
		}
		if (diagnosis.getDoctorUserId() == null) {
			throw new BadRequestException("Diagnosis may contain doctor user id");
		}
		if (diagnosis.getHealthNoteId() == null) {
			throw new BadRequestException("Diagnosis may contain health note id");
		}
		if (diagnosis.getDescription() == null) {
			diagnosis.setDescription(diagnosis.getDescription().trim());
		}
		if (TextUtils.isEmpty(diagnosis.getDescription())) {
			throw new BadRequestException("Diagnosis description is empty");
		}
		if (diagnosis.getMenace() < 1 || diagnosis.getMenace() > 10) {
			throw new BadRequestException("Menace can by from 1 to 10");
		}
		return true;
	}
}
