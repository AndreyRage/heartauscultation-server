<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 26.03.16
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/navbar.jsp"/>
<div class="container">
    <div class="center-content-style">
        <div class="container-body">
            <h4><a href="/health-notes/${note.getId()}">HealthNote #${note.getId()}</a></h4>
            <div class="row note-element">
                <div class="col-sm-4">Record:</div>
                <div class="col-sm-8">
                    <c:if test="${note.getRecFile() != null}">
                        <audio controls>
                            <c:url var="contentUrl" value="/content"/>
                            <source src="${contentUrl}/${note.getRecFile()}">
                            Your browser does not support the audio element.
                        </audio>
                    </c:if>
                </div>
            </div>
            <div class="row note-element">
                <div class="col-sm-4">Blood pressure:</div>
                <div class="col-sm-8"><c:if
                        test="${note.getSystolic() != null}">${note.getSystolic()} / ${note.getDiastolic()}</c:if></div>
            </div>
            <div class="row note-element">
                <div class="col-sm-4">Activity:</div>
                <div class="col-sm-8">${note.getAction()}</div>
            </div>
            <div class="row note-element">
                <div class="col-sm-4">Drugs:</div>
                <div class="col-sm-8">
                    <c:if test="${not empty note.getDrugs()}">
                        <div>
                            <c:forEach var="drug" items="${note.getDrugs()}">
                                <p class="inline">${drug.getName()} ${drug.getDosage()}
                                    <display:column>
                                        <c:choose>
                                            <c:when test="${drug.getMeasure() == 'PCS'}"> pcs</c:when>
                                            <c:when test="${drug.getMeasure() == 'KILO_G'}"> kg</c:when>
                                            <c:when test="${drug.getMeasure() == 'G'}"> g</c:when>
                                            <c:when test="${drug.getMeasure() == 'MILLI_G'}"> mg</c:when>
                                            <c:when test="${drug.getMeasure() == 'MICRO_G'}"> µg</c:when>
                                            <c:when test="${drug.getMeasure() == 'NANO_G'}"> ng</c:when>
                                            <c:when test="${drug.getMeasure() == 'L'}"> l</c:when>
                                            <c:when test="${drug.getMeasure() == 'MILLI_L'}"> ml</c:when>
                                            <c:when test="${drug.getMeasure() == 'MICRO_L'}"> µl</c:when>
                                            <c:when test="${drug.getMeasure() == 'NANO_L'}"> nl</c:when>
                                            <c:otherwise> </c:otherwise>
                                        </c:choose>
                                    </display:column>
                                </p>
                            </c:forEach>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="row note-element">
                <div class="col-sm-4">Description:</div>
                <div class="col-sm-8">${note.getDescription()}</div>
            </div>
            <div class="row note-element">
                <div class="col-sm-4">Time:</div>
                <div class="col-sm-8"><joda:format pattern="yyyy-MM-dd k:mm:ss" value="${note.getCreatedAt()}"/></div>
            </div>
            <c:if test="${pageContext.request.isUserInRole('PATIENT')}">
                <div class="row note-element">
                    <div class="col-sm-12">
                        <div class="note-element">
                            <a class="btn btn-primary" href="/health-notes/edit/${note.getId()}" role="button">Edit</a>
                        </div>
                    </div>
                </div>
            </c:if>
            <div id="diagnosis-container">
                <hr>
                <c:if test="${pageContext.request.isUserInRole('DOCTOR')}">
                    <form class="form-add-diagnosis form-style">
                        <div class="form-group">
                            <h4>Add diagnosis:</h4>
                            <div id="note-alert"></div>
                            <div class="row">
                                <div class="col-sm-3">Description:</div>
                                <div class="col-sm-9 no-padding">
                                    <label for="description" class="sr-only">Diastolic</label>
                                    <textarea id="description" class="form-control" rows="5"
                                              name="description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">Menace:</div>
                                <div class="col-sm-9 no-padding">
                                    <label class="radio-inline"><input type="radio" name="menace" value="1">1</label>
                                    <label class="radio-inline"><input type="radio" name="menace" value="2">2</label>
                                    <label class="radio-inline"><input type="radio" name="menace" value="3">3</label>
                                    <label class="radio-inline"><input type="radio" name="menace" value="4">4</label>
                                    <label class="radio-inline"><input type="radio" name="menace" value="5">5</label>
                                    <label class="radio-inline"><input type="radio" name="menace" value="6">6</label>
                                    <label class="radio-inline"><input type="radio" name="menace" value="7">7</label>
                                    <label class="radio-inline"><input type="radio" name="menace" value="8">8</label>
                                    <label class="radio-inline"><input type="radio" name="menace" value="9">9</label>
                                    <label class="radio-inline"><input type="radio" name="menace" value="10">10</label>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="user-id" name="user-id" value="${pageContext.request.userPrincipal.principal.getId()}"/>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-7 no-padding"></div>
                                <div class="col-sm-5">
                                    <button class="btn btn-lg btn-primary btn-block" type="submit">Add diagnosis</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr>
                </c:if>
                <h4>Diagnosis:</h4>
                <div class="text-center">
                    <c:if test="${not empty diagnosis}">
                        <a id="load-more-diagnosis" class="btn btn-link">Load more</a>
                    </c:if>
                    <c:if test="${empty diagnosis}">
                        <p>There are no diagnosis for this health note.</p>
                    </c:if>
                </div>
                <div id="diagnosis-content">
                    <c:if test="${not empty diagnosis}">
                        <c:forEach var="diagnos" items="${diagnosis}">
                            <div class="diagnos-body">
                                <h5>Diagnosis #${diagnos.getId()} by <a href="/profile/${diagnos.getDoctorUser().getId()}">${diagnos.getDoctorUser().showUsername()}</a></h5>
                                <p class="description">${diagnos.getDescription()}</p>
                                <p class="menace">Menace: ${diagnos.getMenace()}</p>
                                <p class="date"><joda:format pattern="yyyy-MM-dd k:mm:ss"
                                                             value="${diagnos.getCreatedAt()}"/></p>
                                <input type="hidden" class="diagnosis-id" name="diagnosis-id" value="${diagnos.getId()}"/>
                                <c:if test="${pageContext.request.userPrincipal.principal.getId() eq diagnos.getDoctorUserId()}">
                                    <div class="text-center">
                                        <a class="btn btn-sm btn-diagnosis-delete btn-danger form-inline" href="#">Delete</a>
                                        <a class="btn btn-sm btn-success form-inline" href="/diagnosis/${diagnos.getId()}">Edit</a>
                                    </div>
                                </c:if>
                            </div>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var noteId = "${note.getId()}";
</script>

<script src="<c:url value='/static/jquery/jquery-1.12.2.min.js' />"></script>
<script>window.jQuery || document.write('<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"><\/script>')</script>
<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"></script>
<script src="<c:url value='/static/js/diagnosis.js' />"></script>
<script src="<c:url value='/static/js/utils.js' />"></script>

<jsp:include page="include/footer.jsp"/>
