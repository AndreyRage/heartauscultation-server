package ua.org.horishniy.heartauscultation.server.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ua.org.horishniy.heartauscultation.server.enums.Measure;

import javax.persistence.*;

/**
 * Created by rage on 08.03.16.
 */
@Entity
@Table(name = "DRUG")
public class Drug extends AbstractEntity {

    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = "HEALTH_NOTE_ID", nullable = false)
    @JsonIgnore
    private HealthNote mHealthNote;

    @Column(name = "NAME", nullable = false, length = 140)
    @JsonProperty("name")
    private String mName;

    @Column(name = "DOSAGE")
    @JsonProperty("dosage")
    private Float mDosage;

    @Column(name = "MEASURE")
    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("measure")
    private Measure mMeasure;

    public Drug() {
    }

    public Drug(String name, Float dosage, Measure measure) {
        mName = name;
        mDosage = dosage;
        mMeasure = measure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Drug drug = (Drug) o;

        //if (mHealthNote != null ? !mHealthNote.equals(drug.mHealthNote) : drug.mHealthNote != null) return false;
        if (mName != null ? !mName.equals(drug.mName) : drug.mName != null) return false;
        if (mDosage != null ? !mDosage.equals(drug.mDosage) : drug.mDosage != null) return false;
        return mMeasure == drug.mMeasure;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        //result = 31 * result + (mHealthNote != null ? mHealthNote.hashCode() : 0);
        result = 31 * result + (mName != null ? mName.hashCode() : 0);
        result = 31 * result + (mDosage != null ? mDosage.hashCode() : 0);
        result = 31 * result + (mMeasure != null ? mMeasure.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Drug{" +
                "super=" + super.toString() +
                ", mHealthNoteId=" + (mHealthNote != null ? mHealthNote.getId() : "null") +
                ", mName='" + mName + '\'' +
                ", mDosage=" + mDosage +
                ", mMeasure=" + mMeasure +
                '}';
    }

    @JsonIgnore
    public HealthNote getHealthNote() {
        return mHealthNote;
    }

    public void setHealthNote(HealthNote healthNote) {
        mHealthNote = healthNote;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Float getDosage() {
        return mDosage;
    }

    public void setDosage(Float dosage) {
        mDosage = dosage;
    }

    public Measure getMeasure() {
        return mMeasure;
    }

    public void setMeasure(Measure measure) {
        mMeasure = measure;
    }
}
