package ua.org.horishniy.heartauscultation.server.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

public abstract class AbstractDao<PK extends Serializable, T> implements BaseDao<PK, T> {

	private final Class<T> persistentClass;

	@Autowired
	private SessionFactory sessionFactory;

	public AbstractDao(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	protected Criteria createEntityCriteria() {
		return getSession().createCriteria(persistentClass);
	}

	@Override
	@SuppressWarnings("unchecked")
	public T get(PK id) {
		return (T) getSession().get(persistentClass, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public PK add(T object) {
		return (PK) getSession().save(object);
	}

	@Override
	public void update(T object) {
		getSession().update(object);
	}

	@Override
	public void merge(T object) {
		getSession().merge(object);
	}

	@Override
	public void delete(PK id) {
		T object = get(id);
		if (object != null) {
			delete(object);
		}
	}

	@Override
	public void delete(T object) {
		getSession().delete(object);
	}
}