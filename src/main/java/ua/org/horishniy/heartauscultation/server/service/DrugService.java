package ua.org.horishniy.heartauscultation.server.service;

import ua.org.horishniy.heartauscultation.server.entities.Drug;

/**
 * Created by rage on 10.03.16.
 */
public interface DrugService extends BaseService<Long, Drug> {
}
