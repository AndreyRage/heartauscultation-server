package ua.org.horishniy.heartauscultation.server.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import ua.org.horishniy.heartauscultation.server.exceptions.ConflictException;
import ua.org.horishniy.heartauscultation.server.exceptions.ForbidenException;
import ua.org.horishniy.heartauscultation.server.exceptions.NotFoundException;
import ua.org.horishniy.heartauscultation.server.exceptions.ServerErrorException;
import ua.org.horishniy.heartauscultation.server.service.entities.responses.ErrorResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by rage on 05.04.16.
 */
@ControllerAdvice
public class ExceptionControllerAdvice {

	@ExceptionHandler(Exception.class)
	public Object handleAllException(HttpServletRequest request, HttpServletResponse response, Exception e) {
		ErrorResponse errorResponse = new ErrorResponse(e.getMessage());
		if (e instanceof NoHandlerFoundException) {
			errorResponse.setCode(HttpServletResponse.SC_NOT_FOUND);
			errorResponse.setMessage("Resource not found");
		} else if (e instanceof NotFoundException) {
			errorResponse.setCode(HttpServletResponse.SC_NOT_FOUND);
		} else if (e instanceof ConflictException) {
			errorResponse.setCode(HttpServletResponse.SC_CONFLICT);
		} else if (e instanceof ForbidenException) {
			errorResponse.setCode(HttpServletResponse.SC_FORBIDDEN);
		} else if (e instanceof ServerErrorException) {
			errorResponse.setCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} else /*if (e instanceof BadRequestException)*/ {
			errorResponse.setCode(HttpServletResponse.SC_BAD_REQUEST);
		}
		response.setStatus(errorResponse.getCode());
		if (request.getRequestURI().startsWith("/api")) {
			return ResponseEntity.badRequest().body(errorResponse);
		} else {
			ModelAndView view = new ModelAndView("error");
			view.addObject("code", errorResponse.getCode());
			view.addObject("message", errorResponse.getMessage());
			return view;
		}
	}

	@ExceptionHandler(AuthenticationException.class)
	public ResponseEntity handleAuthenticationException() {
		return ResponseEntity.badRequest().body(new ErrorResponse("Bad credential"));
	}

	//Handle in CustomBasicAuthenticationEntryPoint
	/*@ExceptionHandler(value = AccessDeniedException.class)
	public ResponseEntity accessDeniedException(HttpServletRequest req, Exception e) {
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorResponse(e.getMessage()));
	}*/
}
