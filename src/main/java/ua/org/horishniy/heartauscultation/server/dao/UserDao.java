package ua.org.horishniy.heartauscultation.server.dao;

import org.jetbrains.annotations.Nullable;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;

import java.util.List;


/**
 * Created by rage on 09.03.16.
 */
public interface UserDao extends BaseDao<Long, User> {
	@Nullable User getByName(String name);
	boolean isUserNameAlreadyExists(String name);
	List<User> findUsers(String search, Role role);
}
