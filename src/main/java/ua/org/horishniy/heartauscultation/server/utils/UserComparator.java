package ua.org.horishniy.heartauscultation.server.utils;

import ua.org.horishniy.heartauscultation.server.entities.User;

import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by rage on 17.04.16.
 */
public class UserComparator implements Comparator<User> {
	private HashMap<User, Integer> mMap = new HashMap<>();

	public UserComparator(HashMap<User, Integer> map) {
		mMap.putAll(map);
	}

	@Override
	public int compare(User o1, User o2) {
		int val = mMap.get(o2) - mMap.get(o1);
		return val != 0 ? val : -1;
	}
}
