package ua.org.horishniy.heartauscultation.server.service;

import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ua.org.horishniy.heartauscultation.server.configuration.AppInitializer;
import ua.org.horishniy.heartauscultation.server.dao.HealthNoteDao;
import ua.org.horishniy.heartauscultation.server.entities.Drug;
import ua.org.horishniy.heartauscultation.server.entities.HealthNote;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;
import ua.org.horishniy.heartauscultation.server.exceptions.BadRequestException;
import ua.org.horishniy.heartauscultation.server.exceptions.ForbidenException;
import ua.org.horishniy.heartauscultation.server.exceptions.NotFoundException;
import ua.org.horishniy.heartauscultation.server.exceptions.ServerErrorException;
import ua.org.horishniy.heartauscultation.server.utils.TextUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by rage on 12.03.16.
 */
@Service("healthNoteService")
@Transactional
public class HealthNoteServiceImpl extends AbstractService<Long, HealthNote> implements HealthNoteService {

	@Autowired
	HealthNoteServiceImpl(HealthNoteDao dao) {
		super(dao);
	}

	@Override
	public HealthNoteDao getDao() {
		return (HealthNoteDao) super.getDao();
	}

	@Override
	public Long add(HealthNote note) {
		return add(note, null);
	}

	@Override
	public Long add(HealthNote note, @Nullable MultipartFile file) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if (!user.getAuthorities().contains(Role.PATIENT)) {
			throw new ForbidenException("This user can't have health note");
		}
		if (note.getUserId() != null) {
			if (!note.getUserId().equals(user.getId())) {
				throw new ForbidenException("You can't add note for another user");
			}
		} else {
			note.setUserId(user.getId());
		}
		if (note.getDrugs() != null && !note.getDrugs().isEmpty()) {
			for (Drug drug : note.getDrugs()) {
				drug.setHealthNote(note);
			}
		}
		HealthNote existsNote = null;
		if (note.getId() != null) {
			existsNote = get(note.getId());
		}
		if (existsNote != null && !user.getId().equals(existsNote.getUserId())) {
			throw new ForbidenException("You can't edit note for another user");
		}
		if (file != null) {
			note.setRecFile(saveFile(file));
		}
		Long id;
		try {
			if (existsNote != null) {
				existsNote.setAction(note.getAction());
				existsNote.setDevice(note.getDevice());
				existsNote.setSystolic(note.getSystolic());
				existsNote.setDiastolic(note.getDiastolic());
				existsNote.setDescription(note.getDescription());
				existsNote.getDrugs().clear();
				existsNote.getDrugs().addAll(note.getDrugs());
				String existsFile = null;
				if (file != null || note.getRecFile() == null) {
					if (!TextUtils.isEmpty(existsNote.getRecFile())) {
						existsFile = existsNote.getRecFile();
					}
					existsNote.setRecFile(note.getRecFile());
				}
				merge(existsNote);
				if (!TextUtils.isEmpty(existsFile)) {
					deleteFile(existsFile);
				}
				id = -1L;
			} else {
				id = super.add(note);
			}
		} catch (Exception e) {
			deleteFile(note.getRecFile());
			throw e;
		}

		return id;
	}

	@Override
	public void merge(HealthNote healthNote) {
		getDao().merge(healthNote);
	}

	@Override
	public void delete(Long id) {
		HealthNote note = get(id);
		if (note == null) {
			throw new NotFoundException("Health note not found");
		}
		delete(note);
	}

	@Override
	public void delete(HealthNote note) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if (!(user.getAuthorities().contains(Role.PATIENT) || !user.getAuthorities().contains(Role.ADMIN))) {
			throw new ForbidenException("You can't delete health note");
		}
		if (!note.getUserId().equals(user.getId())) {
			throw new ForbidenException("You can't delete note for another user");
		}
		if (note.getRecFile() != null) {
			deleteFile(note.getRecFile());
		}
		super.delete(note);
	}

	@Override
	public List<HealthNote> getBeforeId(@Nullable Long id, int limit) {
		return getDao().getBeforeId(id, limit);
	}

	@Override
	public List<HealthNote> getSinceId(@Nullable Long id, int limit) {
		return getDao().getSinceId(id, limit);
	}

	@Override
	public List<HealthNote> getBeforeId(@Nullable Long id, @Nullable Long userId, int limit) {
		return getDao().getBeforeId(id, userId, limit);
	}

	@Override
	public List<HealthNote> getSinceId(@Nullable Long id, @Nullable Long userId, int limit) {
		return getDao().getSinceId(id, userId, limit);
	}

	@Override
	public List<HealthNote> getBeforeId(@Nullable Long id, @Nullable String search, @Nullable Long userId, int limit) {
		return getDao().getBeforeId(id, TextUtils.removeSpaces(search), userId, limit);
	}

	@Override
	public List<HealthNote> getSinceId(@Nullable Long id, @Nullable String search, @Nullable Long userId, int limit) {
		return getDao().getSinceId(id, TextUtils.removeSpaces(search), userId, limit);
	}

	@Override
	public List<HealthNote> getHealthNotes(boolean before, @Nullable Long id, @Nullable String search, @Nullable Long userId, int limit) {
		if (before)
			return getBeforeId(id, search, userId, limit);
		else
			return getSinceId(id, search, userId, limit);
	}

	@Override
	public List<HealthNote> getHealthNotes(@Nullable Long sinceId, @Nullable Long beforeId, @Nullable String search, @Nullable Long userId, @Nullable Integer limit) {
		if (sinceId != null && beforeId != null) {
			throw new BadRequestException("Can't use since and before ids together");
		}
		boolean before = false;
		Long id = null;
		if (sinceId != null) {
			id = sinceId;
		}
		if (beforeId != null) {
			id = beforeId;
			before = true;
		}
		if (limit == null) {
			limit = 0;
		}
		return getHealthNotes(before, id, search, userId, limit);
	}

	@Override
	public boolean isValid(HealthNote note) {
		if (note != null && note.getDescription() != null) {
			note.setDescription(note.getDescription().trim());
		}
		if (note != null && note.getUserId() != null && (
				!TextUtils.isEmpty(note.getRecFile())
				|| (note.getSystolic() != null && note.getDiastolic() != null)
				|| (note.getDrugs() != null && !note.getDrugs().isEmpty())
				|| !TextUtils.isEmpty(note.getDescription()))) {
			return true;
		}
		throw new BadRequestException("At least one field must be filled");
	}

	private String saveFile(@NotNull MultipartFile file) {
		if (!(file.getContentType().startsWith("audio") || file.getContentType().equals("application/octet-stream"))) {
			throw new BadRequestException("File not audio");
		}

		File uploadRootDir = new File(AppInitializer.FILE_LOCATION);
		if (!uploadRootDir.exists()) {
			uploadRootDir.mkdirs();
		}
		String serverFileName;
		try {
			byte[] bytes = file.getBytes();

			File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + generateFileName(file));

			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();

			serverFileName = serverFile.getName();
		} catch (Exception e) {
			throw new ServerErrorException("Error write file: " + file.getOriginalFilename());
		}
		return serverFileName;
	}

	private boolean deleteFile(String recFile) {
		File uploadRootDir = new File(AppInitializer.FILE_LOCATION);
		File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + recFile);
		return serverFile.exists() && serverFile.delete();
	}

	private String generateFileName(@NotNull MultipartFile file) {
		StringBuilder builder = new StringBuilder();
		builder.append(new Date().getTime());
		builder.append(new Random().nextInt(1000));
		String extension = FilenameUtils.getExtension(file.getOriginalFilename());
		if (!TextUtils.isEmpty(extension)){
			builder.append(".").append(extension);
		}
		return builder.toString();
	}
}
