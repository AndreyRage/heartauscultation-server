package ua.org.horishniy.heartauscultation.server.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

/**
 * Created by rage on 10.03.16.
 */
@Entity
@Table(name = "EXPERIENCE")
public class Experience extends AbstractEntity {

	//@ManyToOne(fetch = FetchType.EAGER)
	//@JoinColumn(name = "USER_ID", nullable = false)
	@Column(name = "USER_ID")
	@JsonProperty("userId")
	private long mUserId;

	@Column(name = "NAME", length = 255, nullable = false)
	@JsonProperty("name")
	private String mName;

	@Column(name = "FROM_YEAR")
	@JsonProperty("fromYear")
	private short mFromYear;

	@Column(name = "FROM_MONTH")
	@JsonProperty("fromMonth")
	private byte mFromMonth;

	@Column(name = "FROM_DAY")
	@JsonProperty("fromDay")
	private byte mFromDay;

	@Column(name = "TO_YEAR")
	@JsonProperty("toYear")
	private short mToYear;

	@Column(name = "TO_MONTH")
	@JsonProperty("toMonth")
	private byte mToMonth;

	@Column(name = "TO_DAY")
	@JsonProperty("toDay")
	private byte mToDay;

	public Experience() {
	}

	public Experience(long userId, String name, short fromYear, byte fromMonth, byte fromDay, short toYear, byte toMonth, byte toDay) {
		mUserId = userId;
		mName = name;
		mFromYear = fromYear;
		mFromMonth = fromMonth;
		mFromDay = fromDay;
		mToYear = toYear;
		mToMonth = toMonth;
		mToDay = toDay;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		Experience that = (Experience) o;

		if (mUserId != that.mUserId) return false;
		if (mFromYear != that.mFromYear) return false;
		if (mFromMonth != that.mFromMonth) return false;
		if (mFromDay != that.mFromDay) return false;
		if (mToYear != that.mToYear) return false;
		if (mToMonth != that.mToMonth) return false;
		if (mToDay != that.mToDay) return false;
		return mName != null ? mName.equals(that.mName) : that.mName == null;

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (int) (mUserId ^ (mUserId >>> 32));
		result = 31 * result + (mName != null ? mName.hashCode() : 0);
		result = 31 * result + (int) mFromYear;
		result = 31 * result + (int) mFromMonth;
		result = 31 * result + (int) mFromDay;
		result = 31 * result + (int) mToYear;
		result = 31 * result + (int) mToMonth;
		result = 31 * result + (int) mToDay;
		return result;
	}

	@Override
	public String toString() {
		return "Experience{" +
				"super=" + super.toString() +
				", mUserId=" + mUserId +
				", mName='" + mName + '\'' +
				", mFromYear=" + mFromYear +
				", mFromMonth=" + mFromMonth +
				", mFromDay=" + mFromDay +
				", mToYear=" + mToYear +
				", mToMonth=" + mToMonth +
				", mToDay=" + mToDay +
				'}';
	}

	public long getUserId() {
		return mUserId;
	}

	public void setUserId(long userId) {
		mUserId = userId;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public short getFromYear() {
		return mFromYear;
	}

	public void setFromYear(short fromYear) {
		mFromYear = fromYear;
	}

	public byte getFromMonth() {
		return mFromMonth;
	}

	public void setFromMonth(byte fromMonth) {
		mFromMonth = fromMonth;
	}

	public byte getFromDay() {
		return mFromDay;
	}

	public void setFromDay(byte fromDay) {
		mFromDay = fromDay;
	}

	public short getToYear() {
		return mToYear;
	}

	public void setToYear(short toYear) {
		mToYear = toYear;
	}

	public byte getToMonth() {
		return mToMonth;
	}

	public void setToMonth(byte toMonth) {
		mToMonth = toMonth;
	}

	public byte getToDay() {
		return mToDay;
	}

	public void setToDay(byte toDay) {
		mToDay = toDay;
	}
}
