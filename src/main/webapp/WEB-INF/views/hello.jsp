<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 26.03.16
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/navbar.jsp"/>
    <h1>Message : ${message}</h1>
    <c:if test="${pageContext.request.remoteUser != null}">
        <p>
            Hello, <b><c:out value="${pageContext.request.remoteUser}"/></b>
        </p>
    </c:if>
<jsp:include page="include/footer.jsp"/>
