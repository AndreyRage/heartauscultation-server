/**
 * Created by rage on 03.04.16.
 */
function handleError(xhr, status) {
    if (status == 'error') {
        if (xhr.status == 0) {
            return ("No Internet connection");
        }
        if (xhr.status == 401) {
            window.location.replace("/sign-in");
        } else {
            try {
                var obj = jQuery.parseJSON(xhr.responseText);
                return (obj.message);
            } catch (e) {
                return ("Something wrong. Status: " + xhr.status);
            }
        }
    } else if (status == "timeout") {
        return ("Timeout");
    }
    return ("Something wrong");
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function scrollTop(div) {
    $('html,body').animate({
        scrollTop: $(div).offset().top
    });
}

function urlParam(name) {
    var results = new RegExp('[\?&]' + name + '=?([^&#]*)').exec(window.location.href);
    if (results != null) {
        return results[1];
    } else {
        return null;
    }
}
