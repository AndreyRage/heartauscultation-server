<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 26.03.16
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/navbar.jsp"/>
<div class="container">
    <div class="center-content-style">
        <div class="container-body">
            <form class="form-edit-diagnosis form-style">
                <div class="form-group">
                    <h4>Edit diagnosis:</h4>
                    <div id="note-alert"></div>
                    <div class="row">
                        <div class="col-sm-3">Description:</div>
                        <div class="col-sm-9 no-padding">
                            <label for="description" class="sr-only">Diastolic</label>
                                    <textarea id="description" class="form-control" rows="5"
                                              name="description">${diagnos.getDescription()}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">Menace:</div>
                        <div class="col-sm-9 no-padding">
                            <label class="radio-inline"><input type="radio" name="menace" value="1" ${diagnos.getMenace() == '1' ? 'checked="checked"' : '' }>1</label>
                            <label class="radio-inline"><input type="radio" name="menace" value="2" ${diagnos.getMenace() == '2' ? 'checked="checked"' : '' }>2</label>
                            <label class="radio-inline"><input type="radio" name="menace" value="3" ${diagnos.getMenace() == '3' ? 'checked="checked"' : '' }>3</label>
                            <label class="radio-inline"><input type="radio" name="menace" value="4" ${diagnos.getMenace() == '4' ? 'checked="checked"' : '' }>4</label>
                            <label class="radio-inline"><input type="radio" name="menace" value="5" ${diagnos.getMenace() == '5' ? 'checked="checked"' : '' }>5</label>
                            <label class="radio-inline"><input type="radio" name="menace" value="6" ${diagnos.getMenace() == '6' ? 'checked="checked"' : '' }>6</label>
                            <label class="radio-inline"><input type="radio" name="menace" value="7" ${diagnos.getMenace() == '7' ? 'checked="checked"' : '' }>7</label>
                            <label class="radio-inline"><input type="radio" name="menace" value="8" ${diagnos.getMenace() == '8' ? 'checked="checked"' : '' }>8</label>
                            <label class="radio-inline"><input type="radio" name="menace" value="9" ${diagnos.getMenace() == '9' ? 'checked="checked"' : '' }>9</label>
                            <label class="radio-inline"><input type="radio" name="menace" value="10" ${diagnos.getMenace() == '10' ? 'checked="checked"' : '' }>10</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 no-padding"></div>
                        <div class="col-sm-4">
                            <button id="btn-delete-diagnosis" class="btn btn-lg btn-danger btn-block">Delete</button>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-lg btn-success btn-block" type="submit">Edit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var diagnosisId = "${diagnos.getId()}";
    var noteId = "${diagnos.getHealthNoteId()}";
</script>

<script src="<c:url value='/static/jquery/jquery-1.12.2.min.js' />"></script>
<script>window.jQuery || document.write('<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"><\/script>')</script>
<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"></script>
<script src="<c:url value='/static/js/diagnosis.js' />"></script>
<script src="<c:url value='/static/js/utils.js' />"></script>

<jsp:include page="include/footer.jsp"/>
