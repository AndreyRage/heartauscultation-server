package ua.org.horishniy.heartauscultation.server.service.entities.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rage on 09.04.16.
 */
public class OkResponse {
	@JsonProperty("message")
	private String mMessage;

	public OkResponse() {
	}

	public OkResponse(String message) {
		mMessage = message;
	}

	public String getMessage() {
		return mMessage;
	}

	public void setMessage(String message) {
		mMessage = message;
	}
}
