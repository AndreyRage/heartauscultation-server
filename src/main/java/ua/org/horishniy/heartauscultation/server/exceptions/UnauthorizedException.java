package ua.org.horishniy.heartauscultation.server.exceptions;

/**
 * Created by rage on 05.04.16.
 */
public class UnauthorizedException extends RuntimeException {
	public UnauthorizedException(String message) {
		super(message);
	}
}
