package ua.org.horishniy.heartauscultation.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.org.horishniy.heartauscultation.server.entities.Diagnosis;
import ua.org.horishniy.heartauscultation.server.entities.Experience;
import ua.org.horishniy.heartauscultation.server.entities.HealthNote;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;
import ua.org.horishniy.heartauscultation.server.exceptions.BadRequestException;
import ua.org.horishniy.heartauscultation.server.exceptions.ForbidenException;
import ua.org.horishniy.heartauscultation.server.exceptions.NotFoundException;
import ua.org.horishniy.heartauscultation.server.service.DiagnosisService;
import ua.org.horishniy.heartauscultation.server.service.ExperienceService;
import ua.org.horishniy.heartauscultation.server.service.HealthNoteService;
import ua.org.horishniy.heartauscultation.server.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * Created by rage on 06.04.16.
 */
@Controller
public class ViewController {

	@Autowired
	private UserService mUserService;

	@Autowired
	private HealthNoteService mHealthNoteService;

	@Autowired
	private DiagnosisService mDiagnosisService;

	@Autowired
	private ExperienceService mExperienceService;

	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public String root(@RequestParam(value = "register", required = false) String register) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null || auth.getName().equals("anonymousUser")) {
			return "redirect:/sign-in";
		}
		if (register != null) {
			return "redirect:/profile";
		}
		return "redirect:/health-notes";
	}

	//LOGIN

	@RequestMapping(value = {"/sign-in"})
	public String signIn() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && !auth.getName().equals("anonymousUser")) {
			return "redirect:/";
		}
		return "sign-in";
	}

	@RequestMapping(value = {"/logout"})
	public String signOut(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			String username = auth.getName();
			new SecurityContextLogoutHandler().logout(request, response, auth);
			return "redirect:/sign-in?logout=" + username;
		}
		return "redirect:/sign-in?logout";
	}

	//USER

	@RequestMapping(value = {"/profile"}, method = RequestMethod.GET)
	public String userEdit(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		model.addAttribute("user", user);
		if (auth.getAuthorities().contains(Role.DOCTOR)) {
			List<Experience> experiences = mExperienceService.getExperienceByUserId(user.getId());
			model.addAttribute("experiences", experiences);
		}
		return "user-edit";
	}

	@RequestMapping(value = {"/profile/{id}"}, method = RequestMethod.GET)
	public String userEdit(Model model, @PathVariable("id") long id) {
		User user = mUserService.get(id);
		if (user == null) {
			throw new NotFoundException("User not found");
		}
		model.addAttribute("user", user);
		if (user.getRole().equals(Role.DOCTOR)) {
			List<Experience> experiences = mExperienceService.getExperienceByUserId(user.getId());
			model.addAttribute("experiences", experiences);
		}
		return "user-view";
	}

	//HEALTH NOTE

	@RequestMapping(value = {"/health-notes"}, method = RequestMethod.GET)
	public String notesView(Model model,
							@RequestParam(value = "sinceId", required = false) Long sinceId,
							@RequestParam(value = "beforeId", required = false) Long beforeId,
							@RequestParam(value = "search", required = false) String search,
							@RequestParam(value = "userId", required = false) Long userId,
							@RequestParam(value = "limit", required = false) Integer limit) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if (userId == null && Role.PATIENT.equals(user.getRole())) {
			userId = user.getId();
		}
		List<HealthNote> list = mHealthNoteService.getHealthNotes(sinceId, beforeId, search, userId, limit);
		model.addAttribute("list", list);
		model.addAttribute("userId", userId);
		return "health-notes";
	}

	@RequestMapping(value = {"/health-notes/{id}"}, method = RequestMethod.GET)
	public String noteView(Model model, @PathVariable("id") long id,
						   @RequestParam(value = "limit", required = false) Integer limit) {
		HealthNote note = mHealthNoteService.get(id);
		if (note == null) {
			throw new NotFoundException("Health note not found");
		}
		int lim = limit != null ? limit : 0;
		List<Diagnosis> diagnosis = mDiagnosisService.getSinceId(null, id, lim);
		Collections.reverse(diagnosis);
		model.addAttribute("note", note);
		model.addAttribute("diagnosis", diagnosis);
		return "health-note-view";
	}

	@RequestMapping(value = {"/health-notes/add"}, method = RequestMethod.GET)
	public String addNoteView() {
		return "health-note-edit";
	}

	@RequestMapping(value = {"/health-notes/edit/{id}"})
	public String editNoteView(Model model, @PathVariable("id") Long id) {
		HealthNote note = mHealthNoteService.get(id);
		if (note == null) {
			throw new NotFoundException("Health not not found");
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if (!user.getId().equals(note.getUserId())) {
			throw new BadRequestException("You can't add note for another user");
		}
		model.addAttribute("note", note);
		return "health-note-edit";
	}

	//DIAGNOSIS

	@RequestMapping(value = {"/diagnosis/{id}"}, method = RequestMethod.GET)
	public String editDiagnosisView(Model model, @PathVariable("id") Long id) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if (!user.getAuthorities().contains(Role.DOCTOR)) {
			throw new ForbidenException("Access denied");
		}
		Diagnosis diagnosis = mDiagnosisService.get(id);
		if (diagnosis == null) {
			throw new NotFoundException("Diagnosis not found");
		}
		if (!user.getId().equals(diagnosis.getDoctorUserId())) {
			throw new BadRequestException("You can't edit diagnosis for another user");
		}
		model.addAttribute("diagnos", diagnosis);
		return "diagnosis-edit";
	}
}
