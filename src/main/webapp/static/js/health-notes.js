/**
 * Created by rage on 10.04.16.
 */

var isLoading = false;
var users = null;
var searchUserId = null;

$(function () {
    $(window).scroll(function () {
        if (!isLoading && $(window).scrollTop() + $(window).height() >= $(document).height() - 100) {
            isLoading = true;
            doLoadMoreNotes();
        }
    });

    $("form.form-search").submit(function (e) {
        e.preventDefault();
        doSearch();
    });

    var timer;
    $("#input-search-user").keyup(function () {
        clearTimeout(timer);
        var val = this.value;
        if (val.length > 0) {
            timer = setTimeout(function () {
                doSearchUser(val);
            }, 400);
        } else {
            searchUserId = null;
            $(".dropdown-search").html('');
        }
    }).focusin(function () {
        $(".dropdown-search").show();
    }).click(function () {
        $(".dropdown-search").show();
    }).focusout(function () {
        setTimeout(function () {
            $(".dropdown-search").hide();
        }, 300);
    });

    $(".dropdown-search").on("click", ".user-autocomplete", function (e) {
        e.preventDefault();
        var index =$ (".dropdown-search a").index(this);
        searchUserId = users[index].id;
        $("#input-search-user").val(formatUserName(users[index]));
        $(".dropdown-search").hide();
        doSearch();
    });
});

function showSearchError(message) {
    if (message == null) {
        $("#alerts-search").html('');
    } else {
        $("#alerts-search").html('<div id="invalid-credentials" class="alert alert-danger"><p>' + message + '</p></div>');
    }
}

function showGetNoteError(message) {
    if (message == null) {
        $("#alerts").html('');
    } else {
        $("#alerts").html('<div id="invalid-credentials" class="alert alert-danger"><p>' + message + '</p></div>');
    }
}

function showMoreButton() {
    $("#alerts").prepend('<a id="load-more-notes" class="btn btn-link">Load more</a>');

    $("#load-more-notes").click(function (e) {
        e.preventDefault();
        doLoadMoreNotes(null);
    });
}

function showLoading() {
    $("#alerts").html('<p>Loading...</p>');
}

function displayNotes(notes, top) {
    var body = notes.html();

    if (top) {
        $(".note-container").prepend(body);
    } else {
        $(".note-container").append(body);
    }
}

function addUser(user) {
    $(".dropdown-search").append('<a class="user-autocomplete">' + formatUserName(user) + '</a>');
}

function formatUserName(user) {
    var name = '(' + user.username + ')';
    if (user.firstName != null) {
        name = name + ' ' + user.firstName;
    }
    if (user.lastName != null) {
        name = name + ' ' + user.lastName;
    }
    return name;
}

function doSearchUser(userSearch) {
    $.ajax({
        url: '/api/users?role=PATIENT&search=' + userSearch,
        type: "GET",
        success: function (data) {
            users = data;
            $(".dropdown-search").html('');
            $.each(data, function () {
                addUser(this);
            });
        },
        error: function (xhr, status, error) {
            showSearchError(handleError(xhr, status));
        }
    });
}

function doSearch() {
    search = $("#input-search").val();
    $(".note-container").html('');
    doLoadMoreNotes();
}

function doLoadMoreNotes() {
    showLoading();

    var limit = urlParam('limit');
    if (limit == null || limit == "") {
        limit = 10;
    }
    var id = $(".note-id").last().val();
    var idPath = '';
    if (id != undefined) {
        idPath = 'beforeId=' + id + '&';
    }
    var userPath = '';
    if (searchUserId != null) {
        userPath = 'userId=' + searchUserId + '&';
    } else if (userId != null && userId != '') {
        userPath = 'userId=' + userId + '&';
    }
    var searchPath = '';
    if (search != null && search != '') {
        searchPath = 'search=' + search + '&';
    }
    $.ajax({
        url: '/health-notes?' + userPath + searchPath + idPath + 'limit=' + limit,
        type: "GET",
        success: function (data) {
            showGetNoteError(null);
            var notes = $(".note-container", $(data));
            displayNotes(notes, false);
            if (notes.children().length == limit) {
                isLoading = false;
            }
        },
        error: function (xhr, status, error) {
            showGetNoteError(handleError(xhr, status));
            showMoreButton();
        }
    });
}