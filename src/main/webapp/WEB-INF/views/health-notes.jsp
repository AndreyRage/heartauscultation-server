<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 26.03.16
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/navbar.jsp"/>
<div class="container">
    <div class="center-content-style">
        <div class="search-container">
            <form class="form-search">
                <div id="alerts-search" class="text-center"></div>
                <div class="row">
                    <c:if test="${!pageContext.request.isUserInRole('PATIENT')}">
                        <div class="col-sm-5">
                            <input type="text" id="input-search-user" class="form-control input-search" autocomplete="off" spellcheck="false">
                            <div class="dropdown-search"></div>
                        </div>
                    </c:if>
                    <div ${pageContext.request.isUserInRole('PATIENT') ? 'class="col-sm-10"' : 'class="col-sm-5"'}>
                        <input type="text" id="input-search" class="form-control input-search" name="search"
                               placeholder="Search" value="${param.search}">
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-block btn-success btn-search">Search</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="note-container">
            <c:if test="${not empty list}">
                <c:forEach var="note" items="${list}">
                    <div class="container-body">
                        <h4><a href="/health-notes/${note.getId()}">HealthNote #${note.getId()}</a></h4>
                        <div class="row note-element">
                            <div class="col-sm-4">Record:</div>
                            <div class="col-sm-8">
                                <c:if test="${note.getRecFile() != null}">
                                    <audio controls>
                                        <c:url var="contentUrl" value="/content"/>
                                        <source src="${contentUrl}/${note.getRecFile()}">
                                        Your browser does not support the audio element.
                                    </audio>
                                </c:if>
                            </div>
                        </div>
                        <div class="row note-element">
                            <div class="col-sm-4">Blood pressure:</div>
                            <div class="col-sm-8"><c:if test="${note.getSystolic() != null}">${note.getSystolic()} / ${note.getDiastolic()}</c:if></div>
                        </div>
                        <div class="row note-element">
                            <div class="col-sm-4">Activity:</div>
                            <div class="col-sm-8">${note.getAction()}</div>
                        </div>
                        <div class="row note-element">
                            <div class="col-sm-4">Drugs:</div>
                            <div class="col-sm-8">
                                <c:if test="${not empty note.getDrugs()}">
                                    <div>
                                        <c:forEach var="drug" items="${note.getDrugs()}">
                                            <p class="inline">${drug.getName()} ${drug.getDosage()}
                                                <display:column>
                                                    <c:choose>
                                                        <c:when test="${drug.getMeasure() == 'PCS'}"> pcs</c:when>
                                                        <c:when test="${drug.getMeasure() == 'KILO_G'}"> kg</c:when>
                                                        <c:when test="${drug.getMeasure() == 'G'}"> g</c:when>
                                                        <c:when test="${drug.getMeasure() == 'MILLI_G'}"> mg</c:when>
                                                        <c:when test="${drug.getMeasure() == 'MICRO_G'}"> µg</c:when>
                                                        <c:when test="${drug.getMeasure() == 'NANO_G'}"> ng</c:when>
                                                        <c:when test="${drug.getMeasure() == 'L'}"> l</c:when>
                                                        <c:when test="${drug.getMeasure() == 'MILLI_L'}"> ml</c:when>
                                                        <c:when test="${drug.getMeasure() == 'MICRO_L'}"> µl</c:when>
                                                        <c:when test="${drug.getMeasure() == 'NANO_L'}"> nl</c:when>
                                                        <c:otherwise> </c:otherwise>
                                                    </c:choose>
                                                </display:column>
                                            </p>
                                        </c:forEach>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                        <div class="row note-element">
                            <div class="col-sm-4">Description:</div>
                            <div class="col-sm-8">${note.getDescription()}</div>
                        </div>
                        <div class="row note-element">
                            <div class="col-sm-4">Time:</div>
                            <div class="col-sm-8"><joda:format pattern="yyyy-MM-dd k:mm:ss" value="${note.getCreatedAt()}" /></div>
                        </div>
                        <input class="note-id" type="hidden" value="${note.getId()}">
                        <c:if test="${pageContext.request.isUserInRole('PATIENT')}">
                            <div class="row note-element">
                                <div class="col-sm-12">
                                    <div class="note-element">
                                        <a class="btn btn-primary" href="/health-notes/edit/${note.getId()}" role="button">Edit</a>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </div>
                </c:forEach>
            </c:if>
        </div>
        <div id="alerts" class="text-center"></div>
    </div>
</div>


<script>
    var userId = "${userId}";
    var search = "${param.search}";
</script>

<script src="<c:url value='/static/jquery/jquery-1.12.2.min.js' />"></script>
<script>window.jQuery || document.write('<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"><\/script>')</script>
<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"></script>
<script src="<c:url value='/static/js/health-notes.js' />"></script>
<script src="<c:url value='/static/js/utils.js' />"></script>

<jsp:include page="include/footer.jsp"/>
