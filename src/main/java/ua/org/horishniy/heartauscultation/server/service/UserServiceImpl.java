package ua.org.horishniy.heartauscultation.server.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.org.horishniy.heartauscultation.server.dao.UserDao;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;
import ua.org.horishniy.heartauscultation.server.exceptions.BadRequestException;
import ua.org.horishniy.heartauscultation.server.exceptions.ConflictException;
import ua.org.horishniy.heartauscultation.server.utils.TextUtils;
import ua.org.horishniy.heartauscultation.server.utils.UserComparator;
import ua.org.horishniy.heartauscultation.server.utils.ValidationUtils;

import java.util.*;

/**
 * Created by rage on 11.03.16.
 */
@Service("userService")
@Transactional
public class UserServiceImpl extends AbstractService<Long, User> implements UserService, UserDetailsService {

	@Autowired
	UserServiceImpl(UserDao dao) {
		super(dao);
	}

	@Override
	public UserDao getDao() {
		return (UserDao) super.getDao();
	}

	@Override
	public Long add(User user) {
		if (!isValid(user)) {
			return null;
		}
		if (isUserNameAlreadyExists(user.getUsername())) {
			throw new ConflictException("Username already exists");
		}
		return getDao().add(user);
	}

	@Override
	public void updateUser(@NotNull User user,@NotNull User existsUser) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getAuthorities().contains(Role.ADMIN)) {
			existsUser.setUsername(user.getUsername());
		}
		existsUser.setEmail(user.getEmail());
		existsUser.setFirstName(user.getFirstName());
		existsUser.setLastName(user.getLastName());
		existsUser.setBirthday(user.getBirthday());

		super.update(existsUser);
	}

	@Override
	public void updatePassword(String currentPassword, String password, String rePassword) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		PasswordEncoder encoder = new BCryptPasswordEncoder();
		if (!encoder.matches(currentPassword, user.getPassword())) {
			throw new BadRequestException("Current password is wrong");
		}
		if (!ValidationUtils.isPasswordValid(password)) {
			throw new BadRequestException("Password is too short (minimum is 6 characters)");
		}
		if (!TextUtils.equals(password, rePassword)) {
			throw new BadRequestException("Password not match");
		}

		user.setPassword(encoder.encode(password));

		getDao().update(user);
	}

	@Override
	public List<User> findUsers(String search, Role role) {
		String formatSearch = TextUtils.removeSpaces(search);
		List<User> users = getDao().findUsers(formatSearch, role);
		if (users.isEmpty()) {
			return users;
		}

		HashMap<User, Integer> userMap = new HashMap<>();
		users.forEach(user -> {
			int k = 0;
			for (String w: formatSearch.split(" ")) {
				if (!TextUtils.isEmpty(user.getUsername())
						&& user.getUsername().toLowerCase().contains(w.toLowerCase())) {
					k++;
				}
				if (!TextUtils.isEmpty(user.getFirstName())
						&& user.getFirstName().toLowerCase().contains(w.toLowerCase())) {
					k++;
				}
				if (!TextUtils.isEmpty(user.getLastName())
						&& user.getLastName().toLowerCase().contains(w.toLowerCase())) {
					k++;
				}
			}
			userMap.put(user, k);
		});

		Comparator<User> comparator = new UserComparator(userMap);
		TreeMap<User, Integer> sortedUserMap = new TreeMap<>(comparator);
		sortedUserMap.putAll(userMap);

		return new ArrayList<>(sortedUserMap.keySet());
	}

	@Nullable
	private User getByName(String name) {
		if (!TextUtils.isEmpty(name))
			return getDao().getByName(name);
		else
			return null;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = getByName(username);
		if (user == null) {
			throw new UsernameNotFoundException(username + " not found");
		}
		return user;
	}

	@Override
	public boolean isUserNameAlreadyExists(String name) {
		return getDao().isUserNameAlreadyExists(name);
	}

	@Override
	public boolean isValid(User user) {
		if (user == null) {
			throw new BadRequestException("User is empty");
		}
		if (user.getUsername() != null) {
			user.setUsername(user.getUsername().trim());
		}
		if (TextUtils.isEmpty(user.getUsername())) {
			throw new BadRequestException("Username is empty");
		}
		if (TextUtils.isEmpty(user.getPassword())) {
			throw new BadRequestException("Password is empty");
		}
		if (TextUtils.isEmpty(user.getEmail())) {
			throw new BadRequestException("Email is empty");
		}
		if (user.getRole() == null) {
			throw new BadRequestException("Role is empty");
		}
		if (!ValidationUtils.isUserNameValid(user.getUsername())) {
			throw new BadRequestException("Username is not valid (minimum is 3 characters, maximum 20 characters)");
		}
		if (!ValidationUtils.isEmailValid(user.getEmail())) {
			throw new BadRequestException("Email is not valid");
		}
		return true;
	}
}
