<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 03.04.16
  Time: 21:42
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Kickstart My Heart<c:if test="${title != null}"> - ${title}</c:if></title>
    <link href="<c:url value='/static/bootstrap/css/bootstrap.min.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/css.css' />" rel="stylesheet"/>

    <c:url var="page" value="${requestScope['javax.servlet.forward.request_uri']}"/>
    <c:if test="${page == '/sign-in'}">
        <link href="<c:url value='/static/css/sign-in.css' />" rel="stylesheet"/>
    </c:if>
    <c:if test="${page == '/profile'}">
        <link href="<c:url value='/static/datepicker/css/datepicker.css' />" rel="stylesheet"/>
    </c:if>
    <c:if test="${code != null}">
        <link href="<c:url value='/static/css/error.css' />" rel="stylesheet"/>
    </c:if>
</head>

<body>
