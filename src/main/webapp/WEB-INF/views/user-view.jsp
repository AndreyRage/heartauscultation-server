<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 26.03.16
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/navbar.jsp"/>
<div class="container">
    <div class="center-content-style">
        <div class="container-body">
            <h2 class="container-title">User profile:</h2><div class="form-group">
            <div class="row">
                <div class="col-sm-4 no-padding">Username:</div>
                <div class="col-sm-8 no-padding">${user.getUsername()}</div>
            </div>
        </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4 no-padding">First name:</div>
                    <div class="col-sm-8 no-padding">${user.getFirstName()}</div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4 no-padding">Last name:</div>
                    <div class="col-sm-8 no-padding">${user.getLastName()}</div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4 no-padding">Email:</div>
                    <div class="col-sm-8 no-padding">${user.getEmail()}</div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4 no-padding">Birthday:</div>
                    <div class="col-sm-8 no-padding">
                        <joda:format pattern="MM/dd/yyyy" value="${user.getBirthday()}"/>
                    </div>
                </div>
            </div>
        </div>

        <c:if test="${not empty experiences}">
            <div class="container-body">
                <h2 class="container-title">Experience:</h2>
                <c:forEach var="experience" items="${experiences}">
                    <div class="form-group experience-group-item">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-1 no-padding">Name:</div>
                                    <div class="col-sm-11 no-padding">${experience.getName()}</div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-1 no-padding text-center"></div>
                                    <div class="col-sm-11 no-padding">From:
                                    ${experience.getFromYear() != 0 ? experience.getFromYear() : ''}
                                    ${experience.getFromMonth() != 0 ? experience.getFromMonth() : ''}
                                    ${experience.getFromDay() != 0 ? experience.getFromDay() : ''}
                                    -
                                    To:
                                    ${experience.getToYear() != 0 ? experience.getToYear() : ''}
                                    ${experience.getToMonth() != 0 ? experience.getToMonth() : ''}
                                    ${experience.getToDay() != 0 ? experience.getToDay() : ''}
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </c:if>

    </div>
</div>

<jsp:include page="include/footer.jsp"/>
