package ua.org.horishniy.heartauscultation.server.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

/**
 * Created by rage on 10.03.16.
 */
@Entity
@Table(name = "DIAGNOSIS")
public class Diagnosis extends AbstractEntity {

	/*@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name = "HEALTH_NOTE_ID", nullable = false)
	private HealthNote mHealthNote;*/

	@Column(name = "HEALTH_NOTE_ID", nullable = false)
	@JsonProperty("healthNoteId")
	private Long mHealthNoteId;

	/*@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name = "DOCTOR_USER_ID", nullable = false)
	private User mDoctorUser;*/

	@Column(name = "DOCTOR_USER_ID", nullable = false)
	@JsonProperty("doctorUserId")
	private Long mDoctorUserId;

	@Column(name = "DESCRIPTION", nullable = false)
	@JsonProperty("description")
	private String mDescription;

	@Column(name = "MENACE", nullable = false)
	@JsonProperty("menace")
	private byte mMenace;

	@Transient
	@JsonProperty("doctorUser")
	private User mDoctorUser;

	public Diagnosis() {
	}

	public Diagnosis(Long healthNoteId, Long doctorUserId, String description, byte menace) {
		mHealthNoteId = healthNoteId;
		mDoctorUserId = doctorUserId;
		mDescription = description;
		mMenace = menace;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		Diagnosis diagnosis = (Diagnosis) o;

		if (mMenace != diagnosis.mMenace) return false;
		if (mHealthNoteId != null ? !mHealthNoteId.equals(diagnosis.mHealthNoteId) : diagnosis.mHealthNoteId != null)
			return false;
		if (mDoctorUserId != null ? !mDoctorUserId.equals(diagnosis.mDoctorUserId) : diagnosis.mDoctorUserId != null)
			return false;
		return mDescription != null ? mDescription.equals(diagnosis.mDescription) : diagnosis.mDescription == null;

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (mHealthNoteId != null ? mHealthNoteId.hashCode() : 0);
		result = 31 * result + (mDoctorUserId != null ? mDoctorUserId.hashCode() : 0);
		result = 31 * result + (mDescription != null ? mDescription.hashCode() : 0);
		result = 31 * result + (int) mMenace;
		return result;
	}

	@Override
	public String toString() {
		return "Diagnosis{" +
				"super=" + super.toString() +
				", mHealthNoteId=" + mHealthNoteId +
				", mDoctorUserId=" + mDoctorUserId +
				", mDescription='" + mDescription + '\'' +
				", mMenace=" + mMenace +
				'}';
	}

	public Long getHealthNoteId() {
		return mHealthNoteId;
	}

	public void setHealthNoteId(Long healthNoteId) {
		mHealthNoteId = healthNoteId;
	}

	public Long getDoctorUserId() {
		return mDoctorUserId;
	}

	public void setDoctorUserId(Long doctorUserId) {
		mDoctorUserId = doctorUserId;
	}

	public String getDescription() {
		return mDescription;
	}

	public void setDescription(String description) {
		mDescription = description;
	}

	public byte getMenace() {
		return mMenace;
	}

	public void setMenace(byte menace) {
		mMenace = menace;
	}

	@JsonIgnoreProperties("doctorUser")
	public User getDoctorUser() {
		return mDoctorUser;
	}

	public void setDoctorUser(User doctorUser) {
		mDoctorUser = doctorUser;
	}
}
