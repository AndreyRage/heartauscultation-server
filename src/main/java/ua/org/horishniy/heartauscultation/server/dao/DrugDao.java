package ua.org.horishniy.heartauscultation.server.dao;

import ua.org.horishniy.heartauscultation.server.entities.Drug;

/**
 * Created by rage on 10.03.16.
 */
public interface DrugDao extends BaseDao<Long, Drug> {
}
