package ua.org.horishniy.heartauscultation.server.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

@MappedSuperclass
public abstract class AbstractEntity {
    @Id
    @Column(name = "ID")
    @JsonProperty("id")
    @GeneratedValue
    private Long mId;

    //SQL TIMESTAMP TYPE
    @Column(name="CREATED_AT")
    @JsonIgnore
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    public DateTime mCreatedAt;

    @Column(name="UPDATED_AT")
    @JsonIgnore
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    public DateTime mUpdatedAt;

    @PrePersist
    protected void onCreate() {
        mCreatedAt = new DateTime();
    }

    @PreUpdate
    protected void onUpdate() {
        mUpdatedAt = new DateTime();
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    @JsonIgnoreProperties("updatedAt")
    public DateTime getCreatedAt() {
        return mCreatedAt;
    }

    @JsonIgnoreProperties("updatedAt")
    public DateTime getUpdatedAt() {
        return mUpdatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractEntity)) return false;

        AbstractEntity that = (AbstractEntity) o;

        if (mId != null ? !mId.equals(that.mId) : that.mId != null) return false;
        if (mCreatedAt != null ? !mCreatedAt.equals(that.mCreatedAt) : that.mCreatedAt != null) return false;
        return mUpdatedAt != null ? mUpdatedAt.equals(that.mUpdatedAt) : that.mUpdatedAt == null;

    }

    @Override
    public int hashCode() {
        int result = mId != null ? mId.hashCode() : 0;
        result = 31 * result + (mCreatedAt != null ? mCreatedAt.hashCode() : 0);
        result = 31 * result + (mUpdatedAt != null ? mUpdatedAt.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AbstractEntity{" +
                "mId=" + mId +
                ", mCreatedAt=" + mCreatedAt +
                ", mUpdatedAt=" + mUpdatedAt +
                '}';
    }
}