/**
 * Created by rage on 10.04.16.
 */

$(function () {
    $("form.form-user-edit").submit(function (e) {
        e.preventDefault();
        doEditUser();
    });
    $("form.form-password-edit").submit(function (e) {
        e.preventDefault();
        doChangePassword();
    });
    $("form.form-experience-edit").submit(function (e) {
        e.preventDefault();
        doUpdateExperience();
    });
    $('#birthday').datepicker();

    initExperience();
});

var deleteExperience = [];

function initExperience() {
    var wrapper = $("#experience-group");

    var x = $(".experience-group-item").length;

    $("#experience-group-button-add").click(function (e) {
        e.preventDefault();
        addExperienceField();
        x++;
    });

    $(wrapper).on("click", ".experience-group-delete", function (e) {
        e.preventDefault();
        var id = $(".experience-id", $(this).parent('div').parent('div')).val();
        $(this).parent('div').parent('div').parent('div').remove();
        x--;
        if (x == 0) {
            addExperienceField();
            x++;
        }
        if (id != '') {
            deleteExperience.push(id);
        }
    });
}

function addExperienceField(experience) {
    if (experience == null) {
       experience = new ExperienceObj();
    } else {
        if (experience.fromYear == 0) {
            experience.fromYear = '';
        }
        if (experience.fromMonth == 0) {
            experience.fromMonth = '';
        }
        if (experience.fromDay == 0) {
            experience.fromDay = '';
        }
        if (experience.toYear == 0) {
            experience.toYear = '';
        }
        if (experience.toMonth == 0) {
            experience.toMonth = '';
        }
        if (experience.toDay == 0) {
            experience.toDay = '';
        }
    }
    $("#experience-group").append(
        '<div class="form-group experience-group-item"> <div class="row"> <div class="col-sm-11">' +
        '<input type="hidden" class="experience-id" name="experience-id" value="' + experience.id +
        '"/> <div class="row"> <div class="col-sm-1 no-padding">Name:</div> <div class="col-sm-11 no-padding">' +
        '<input type="text" class="form-control" name="name" placeholder="Last name" value="' + experience.name +
        '"> </div> </div> <div class="row"> <div class="col-sm-1 no-padding text-center"></div>' +
        '<div class="col-sm-1 no-padding">From:</div> <div class="col-sm-2 no-padding">' +
        '<input type="text" class="form-control" name="fromYear" placeholder="Year" value="' + experience.fromYear +
        '"> </div> <div class="col-sm-1 no-padding"> <input type="text" class="form-control" name="fromMonth"' +
        'placeholder="M" value="' + experience.fromMonth + '"> </div> <div class="col-sm-1 no-padding">' +
        '<input type="text" class="form-control" name="fromDay" placeholder="D" value="' + experience.fromDay +
        '"> </div> <div class="col-sm-1 no-padding text-center">-</div> <div class="col-sm-1 no-padding">To:</div>' +
        '<div class="col-sm-2 no-padding"> <input type="text" class="form-control" name="toYear" placeholder="Year"' +
        'value="' + experience.toYear + '"> </div> <div class="col-sm-1 no-padding"><input type="text"' +
        'class="form-control" name="toMonth" placeholder="M" value="' + experience.toMonth +
        '"> </div> <div class="col-sm-1 no-padding"> <input type="text" class="form-control" name="toDay"' +
        'placeholder="D" value="' + experience.toDay + '"> </div> </div> </div> <div class="col-sm-1">' +
        '<a class="btn btn-default drug-group-delete experience-group-delete">Delete</a> </div> </div> </div>'
    );
}

function showUserEditError(message) {
    if (message == null) {
        $("#user-alert").html('');
    } else {
        $("#user-alert").html('<div id="invalid-credentials" class="alert alert-danger"><p>' + message + '</p></div>');
    }
}

function showUserEditSuccess(message) {
    if (message == null) {
        $("#user-alert").html('');
    } else {
        $("#user-alert").html('<div id="success-credentials" class="alert alert-success"><p>' + message + '</p></div>');
    }
}

function showExperienceError(message) {
    if (message == null) {
        $("#experience-alert").html('');
    } else {
        $("#experience-alert").html('<div id="invalid-credentials" class="alert alert-danger"><p>' + message + '</p></div>');
    }
}

function showExperienceSuccess(message) {
    if (message == null) {
        $("#experience-alert").html('');
    } else {
        $("#experience-alert").html('<div id="success-credentials" class="alert alert-success"><p>' + message + '</p></div>');
    }
}

function showPasswordEditError(message) {
    if (message == null) {
        $("#password-alert").html('');
    } else {
        $("#password-alert").html('<div id="invalid-credentials" class="alert alert-danger"><p>' + message + '</p></div>');
    }
}

function showPasswordEditSuccess(message) {
    if (message == null) {
        $("#password-alert").html('');
    } else {
        $("#password-alert").html('<div id="invalid-credentials" class="alert alert-success"><p>' + message + '</p></div>');
    }
}

function doEditUser() {
    var birthday = $("#birthday").val();

    var user = new UserObj();
    user.email = $("#email").val();
    user.firstName = $("#firstname").val();
    user.lastName = $("#lastname").val();

    if (user.firstName == '') {
        user.firstName = null;
    }

    if (user.lastName == '') {
        user.lastName = null;
    }

    if (birthday != '') {
        try {
            var date = new Date(birthday + ' 00:00:00 GMT');
            user.birthday = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        } catch (e) {
            showUserEditError("Birthday is not valid");
        }
    }

    if (!isEmail(user.email)) {
        showUserEditError("Email is not valid");
    }

    $.ajax({
        url: '/api/users/my',
        type: "PUT",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(user),
        success: function () {
            showUserEditSuccess("Profile successfully updated");
        },
        error: function (xhr, status, error) {
            showUserEditError(handleError(xhr, status));
        }
    });
}

function doChangePassword() {
    var currentPassword = $("#current-password").val();
    var password = $("#new-password").val();
    var rePassword = $("#confirm-password").val();

    if (currentPassword == '') {
        showPasswordEditError("Enter current password");
    }

    if (password != rePassword) {
        showPasswordEditError("Password not match");
    }

    $.ajax({
        url: '/api/users/my/password',
        type: "PUT",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('currentPassword', currentPassword);
            xhr.setRequestHeader('password', password);
            xhr.setRequestHeader('rePassword', rePassword);
        },
        success: function () {
            showPasswordEditSuccess("Password successfully updated");
        },
        error: function (xhr, status, error) {
            showPasswordEditError(handleError(xhr, status));
        }
    });
}

function doUpdateExperience() {
    var experiences = [];

    $("#experience-group .experience-group-item").each(function () {
        var experience = new ExperienceObj();
        experience.id = $("input[name='experience-id']", this).val();
        experience.name = $("input[name='name']", this).val();
        experience.fromYear = $("input[name='fromYear']", this).val();
        experience.fromMonth = $("input[name='fromMonth']", this).val();
        experience.fromDay = $("input[name='fromDay']", this).val();
        experience.toYear = $("input[name='toYear']", this).val();
        experience.toMonth = $("input[name='toMonth']", this).val();
        experience.toDay = $("input[name='toDay']", this).val();

        if (experience.name != "") {
            experiences.push(experience);
        }
    });

    $.ajax({
        url: '/api/users/my/experiences',
        type: "PUT",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(experiences),
        success: function (data) {
            if (deleteExperience.length > 0) {
                doDeleteExperience();
            } else {
                $("#experience-group").html('');
                $.each(data, function () {
                    addExperienceField(this);
                });
                showExperienceSuccess("Experience successfully updated");
            }
        },
        error: function (xhr, status, error) {
            showExperienceError(handleError(xhr, status));
        }
    });
}

function doDeleteExperience() {
    $.ajax({
        url: '/api/users/my/experiences',
        type: "DELETE",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(deleteExperience),
        success: function (data) {
            $("#experience-group").html('');
            $.each(data, function () {
                addExperienceField(this);
            });
            deleteExperience = [];
            showExperienceSuccess("Experience successfully updated");
        },
        error: function (xhr, status, error) {
            showExperienceError(handleError(xhr, status));
        }
    });
}

function UserObj () {
    this.email = null;
    this.firstName = null;
    this.lastName = null;
    this.birthday = null;
}

function ExperienceObj () {
    this.id = '';
    this.name = '';
    this.fromYear = '';
    this.fromMonth = '';
    this.fromDay = '';
    this.toYear = '';
    this.toMonth = '';
    this.toDay = '';
}