package ua.org.horishniy.heartauscultation.server.service.entities.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rage on 03.04.16.
 */
public class CheckUsernameResponse {
	@JsonProperty("isUsernameExists")
	private boolean mIsUsernameExists;

	public CheckUsernameResponse() {
	}

	public CheckUsernameResponse(boolean isUsernameExists) {
		this.mIsUsernameExists = isUsernameExists;
	}

	@JsonIgnore
	public boolean isUsernameExists() {
		return mIsUsernameExists;
	}

	public void setUsernameExists(boolean usernameExists) {
		mIsUsernameExists = usernameExists;
	}
}
