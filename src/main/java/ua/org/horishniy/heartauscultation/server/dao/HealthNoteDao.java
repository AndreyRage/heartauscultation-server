package ua.org.horishniy.heartauscultation.server.dao;

import org.jetbrains.annotations.Nullable;
import ua.org.horishniy.heartauscultation.server.entities.HealthNote;

import java.util.List;

/**
 * Created by rage on 09.03.16.
 */
public interface HealthNoteDao extends BaseDao<Long, HealthNote> {
	List<HealthNote> getBeforeId(@Nullable Long id, int limit);
	List<HealthNote> getSinceId(@Nullable Long id, int limit);
	List<HealthNote> getBeforeId(@Nullable Long id, @Nullable Long userId, int limit);
	List<HealthNote> getSinceId(@Nullable Long id, @Nullable Long userId, int limit);
	List<HealthNote> getBeforeId(@Nullable Long id, @Nullable String search, @Nullable Long userId, int limit);
	List<HealthNote> getSinceId(@Nullable Long id, @Nullable String search, @Nullable Long userId, int limit);
}
