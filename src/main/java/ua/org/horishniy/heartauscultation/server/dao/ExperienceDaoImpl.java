package ua.org.horishniy.heartauscultation.server.dao;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;
import ua.org.horishniy.heartauscultation.server.entities.Experience;

import java.util.List;

/**
 * Created by rage on 10.03.16.
 */
@Repository("experienceDao")
public class ExperienceDaoImpl extends AbstractDao<Long, Experience> implements ExperienceDao {

	public ExperienceDaoImpl() {
		super(Experience.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Experience> getExperienceByUserId(Long userId) {
		SQLQuery query = getSession().createSQLQuery("SELECT * FROM EXPERIENCE WHERE USER_ID = :userId");
		query.setLong("userId", userId);
		return (List<Experience>) query.addEntity(Experience.class).list();
	}

	@Override
	public void addExperience(List<Experience> experience) {
		experience.forEach(exp -> {
			if (exp.getId() == null) {
				getSession().saveOrUpdate(exp);
			} else {
				getSession().merge(exp);
			}
		});
	}

	@Override
	public void deleteExperience(List<Long> listId) {
		listId.forEach(this::delete);
	}
}
