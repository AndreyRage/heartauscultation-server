package ua.org.horishniy.heartauscultation.server.exceptions;

/**
 * Created by rage on 05.04.16.
 */
public class ServerErrorException extends RuntimeException {
	public ServerErrorException(String message) {
		super(message);
	}
}
