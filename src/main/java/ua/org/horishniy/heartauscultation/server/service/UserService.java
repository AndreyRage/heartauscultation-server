package ua.org.horishniy.heartauscultation.server.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.userdetails.UserDetailsService;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;

import java.util.List;

/**
 * Created by rage on 11.03.16.
 */
public interface UserService extends BaseService<Long, User>, UserDetailsService {
	boolean isUserNameAlreadyExists(String name);
	void updateUser(@NotNull User user,@NotNull User existsUser);
	void updatePassword(String currentPassword, String password, String rePassword);
	List<User> findUsers(String search, Role role);
}
