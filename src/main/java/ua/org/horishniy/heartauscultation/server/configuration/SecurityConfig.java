package ua.org.horishniy.heartauscultation.server.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ua.org.horishniy.heartauscultation.server.utils.CustomAccessDeniedHandler;
import ua.org.horishniy.heartauscultation.server.utils.CustomBasicAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

	@Autowired
	//@Qualifier("userService")
	private UserDetailsService mUserDetailsService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(mUserDetailsService).passwordEncoder(passwordEncoder());
		//auth.inMemoryAuthentication().withUser("admin").password("password").roles("ADMIN");
	}

	@Bean
	public PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	@Configuration
	@Order(1)
	public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

		@Autowired
		private CustomAccessDeniedHandler mAccessDeniedHandler;

		@Bean
		public CustomBasicAuthenticationEntryPoint mEntryPoint() {
			CustomBasicAuthenticationEntryPoint entryPoint = new CustomBasicAuthenticationEntryPoint();
			entryPoint.setRealmName("realmName");
			return entryPoint;
		}

		@Bean(name="authenticationManager")
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}

		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
					.antMatcher("/api/**")
					.authorizeRequests()
						.antMatchers("/api/login").permitAll()
						.antMatchers("/api/register").permitAll()
						.antMatchers("/api/check-username").permitAll()
						.antMatchers(HttpMethod.PUT ,"api/users/(\\d+)").access("hasRole('ADMIN')")
						.antMatchers(HttpMethod.PUT ,"api/users/(\\d+)/**").access("hasRole('ADMIN')")
						.anyRequest().not().anonymous()
						.and()
					.exceptionHandling()
						.authenticationEntryPoint(mEntryPoint())
						.accessDeniedHandler(mAccessDeniedHandler)
			;
		}

		@Override
		public void configure(WebSecurity web) throws Exception {
			web.ignoring()
					.antMatchers("/api/error/**");
		}
	}

	@Configuration
	public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.anonymous().disable()
					.authorizeRequests()
						.antMatchers("/content/**").permitAll()
						.antMatchers("/admin/**").access("hasRole('ADMIN')")
						.antMatchers("/health-notes/add").access("hasRole('PATIENT')")
						.antMatchers("/health-notes/edit/**").access("hasRole('PATIENT')")
						.antMatchers("/health-notes/**").permitAll()
						.antMatchers("/health-notes").permitAll()
						.antMatchers("/profile/**").permitAll()
						.antMatchers("/diagnosis/**").access("hasRole('DOCTOR')")
						.and()
					.formLogin()
						.loginPage("/sign-in");
		}

		@Override
		public void configure(WebSecurity web) throws Exception {
			web.ignoring()
					.antMatchers("/static/**");
		}
	}
}