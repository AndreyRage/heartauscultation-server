package ua.org.horishniy.heartauscultation.server.enums;

/**
 * Created by rage on 09.03.16.
 */
public enum Action {
    REST, WARM_UP, RUN, EXTREME
}
