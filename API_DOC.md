# Hearth auscultation

## Session

* `201` - added

* `401` - unauthorized
* `400` - something wrong with request

### POST /api/login
Request:
Headers:
Authorization       BasicAuth
Response:
    200
    {
        "id": 1,
        "username": "admin",
        "email": "email@mail.com",
        "role": "ADMIN",
        ...
        "createdAt": "2016-03-12T10:56:03.000Z",
        "updatedAt": "2016-04-03T07:54:40.000Z"
    }
    or:
    401

### POST api/register
Request:
Headers:
Authorization       BasicAuth
Response:
    201
    {
        "id": 1,
        "username": "admin",
        "email": "email@mail.com",
        "role": "ADMIN",
        ...
        "createdAt": "2016-03-12T10:56:03.000Z",
        "updatedAt": "2016-04-03T07:54:40.000Z"
    }
    or:
    400
    {
        "message":"error message"
    }
    or:
    401
    {
        "message":"Bad credentials"
    }