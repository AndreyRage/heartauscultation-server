package ua.org.horishniy.heartauscultation.server.service;

import java.io.Serializable;

/**
 * Created by rage on 11.03.16.
 */
public interface BaseService<PK extends Serializable, T> {
	T get(PK id);
	PK add(T object);
	void update(T object);
	void delete(T object);
	void delete(PK id);
	boolean isValid(T object);
}
