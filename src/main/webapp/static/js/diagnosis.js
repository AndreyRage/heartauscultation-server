/**
 * Created by rage on 10.04.16.
 */
$(function () {
    $("form.form-add-diagnosis").submit(function (e) {
        e.preventDefault();
        doAddDiagnosis();
    });
    $("form.form-edit-diagnosis").submit(function (e) {
        e.preventDefault();
        doEditDiagnosis();
    });
    $("#btn-delete-diagnosis").click(function (e) {
        e.preventDefault();
        doDeleteDiagnosis(null);
    });
    $("#load-more-diagnosis").click(function (e) {
        e.preventDefault();
        doLoadMoreDiagnosis();
    });
    $("#diagnosis-content").on("click", ".btn-diagnosis-delete", function (e) {
        e.preventDefault();
        doDeleteDiagnosis($(this).parent('div').parent('div'));
    });
});

function showAddDiagnosisError(message) {
    if (message == null) {
        $("#note-alert").html('');
    } else {
        $("#note-alert").html('<div id="invalid-credentials" class="alert alert-danger"><p>' + message + '</p></div>');
    }
}

function addDiagnosis(diagnosis, top) {
    var user = ' by ';
    if (diagnosis.doctorUser != null) {
        user = user + '<a href="/profile/">';
        if (diagnosis.doctorUser.firstName != null) {
            user = user + diagnosis.doctorUser.firstName;
            if (diagnosis.doctorUser.lastName != null) {
                user = user + ' ' + diagnosis.doctorUser.lastName;
            }
        } else {
            user = user + diagnosis.doctorUser.username;
        }
        user = user + '</a>';
    }

    var date;
    if (diagnosis.createdAt != null) {
        date = new Date(diagnosis.createdAt);
    } else {
        date = new Date();
    }
    var dateString =
        date.getFullYear() +"-"+
        ("0" + (date.getMonth()+1)).slice(-2) +"-"+
        ("0" + date.getDate()).slice(-2) + " " +
        date.getHours() + ":" +
        ("0" + date.getMinutes()).slice(-2) + ":" +
        ("0" + date.getSeconds()).slice(-2);

    var userId = $("#user-id").val();

    var body = '<div class="diagnos-body"><h5>Diagnosis #' + diagnosis.id + user
        + '</h5><p class="description">' + diagnosis.description
        + '</p><p class="menace">Menace: ' + diagnosis.menace
        + '</p><p class="date">' + dateString + '</p><input type="hidden" class="diagnosis-id" name="diagnosis-id" value="'
        + diagnosis.id + '"/>';

    if (userId == diagnosis.doctorUserId) {
        var buttons = '<div class="text-center"><a class="btn btn-sm btn-diagnosis-delete btn-danger form-inline" href="#">Delete</a> '
            + '<a class="btn btn-sm btn-success form-inline" href="/diagnosis/' + diagnosis.id + '">Edit</a> </div>';
        body = body + buttons;
    }

    body = body + '</div>';

    if (top) {
        $("#diagnosis-content").prepend(body);
    } else {
        $("#diagnosis-content").append(body);
    }
}

function doAddDiagnosis() {
    showAddDiagnosisError(null);

    var fDescription = $("#description");
    var fMenace = $("input[name=menace]:checked");

    var diagnosis = new DiagnosisObj;

    diagnosis.menace = fMenace.val();
    diagnosis.description = fDescription.val();

    if (diagnosis.menace == null
        || diagnosis.description == "") {
        showAddDiagnosisError("All field must be filled");
        return;
    }

    $.ajax({
        url: '/api/note/' + noteId + '/diagnosis',
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(diagnosis),
        success: function (data) {
            fDescription.val("");
            fMenace.removeAttr("checked");
            showAddDiagnosisError();

            addDiagnosis(data, false);

            scrollTop($('.diagnos-body').last());
        },
        error: function (xhr, status, error) {
            showAddDiagnosisError(handleError(xhr, status));
        }
    });
}

function doEditDiagnosis() {
    showAddDiagnosisError(null);

    var fDescription = $("#description");
    var fMenace = $("input[name=menace]:checked");

    var diagnosis = new DiagnosisObj;

    diagnosis.menace = fMenace.val();
    diagnosis.description = fDescription.val();

    if (diagnosis.menace == null
        || diagnosis.description == "") {
        showAddDiagnosisError("All field must be filled");
        return;
    }

    $.ajax({
        url: '/api/diagnosis/' + diagnosisId,
        type: "PUT",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(diagnosis),
        success: function () {
            window.history.back();
        },
        error: function (xhr, status, error) {
            showAddDiagnosisError(handleError(xhr, status));
        }
    });
}

function doLoadMoreDiagnosis() {
    showAddDiagnosisError(null);

    var id = $(".diagnosis-id").first().val();
    var limit = urlParam('limit');
    if (limit == null || limit == "") {
        limit = 10;
    }
    $.ajax({
        url: '/api/note/' + noteId + '/diagnosis?beforeId=' + id + '&limit=' + limit,
        type: "GET",
        success: function (data) {
            var windowHeight = $(document).height();
            $.each(data, function () {
                addDiagnosis(this, true);
            });
            if (data.length < limit) {
                $("#load-more-diagnosis").hide();
            }
            $(document).scrollTop($(document).scrollTop() + $(document).height() - windowHeight);
        },
        error: function (xhr, status, error) {
            showAddDiagnosisError(handleError(xhr, status));
        }
    });
}

function doDeleteDiagnosis(view) {
    showAddDiagnosisError(null);

    var id;
    if (view == null) {
        id = diagnosisId;
    } else {
        id = $(".diagnosis-id", view).val();
    }
    $.ajax({
        url: '/api/diagnosis/' + id,
        type: "DELETE",
        success: function () {
            if (view == null) {
                window.history.back();
            } else {
                view.remove();
            }
        },
        error: function (xhr, status, error) {
            showAddDiagnosisError(handleError(xhr, status));
        }
    });
}

function DiagnosisObj () {
    this.menace = null;
    this.description = null;
}