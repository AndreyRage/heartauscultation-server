<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 03.04.16
  Time: 22:22
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <c:url var="baseUrl" value="/"/>
            <a class="navbar-brand" href="${baseUrl}">Kickstart My Heart</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <c:url var="page" value="${requestScope['javax.servlet.forward.request_uri']}"/>
                <sec:authorize access="isAuthenticated()" var="isAuthenticated" />
                <c:if test="${isAuthenticated}">
                    <c:url var="notesUrl" value="/health-notes"/>
                    <li id="nav-note" ${page == notesUrl ? 'class="active"' : '' }><a href="${notesUrl}">Health Notes</a></li>
                    <sec:authorize access="hasRole('PATIENT')">
                        <c:url var="addNoteUrl" value="/health-notes/add"/>
                        <li id="nav-note-add" ${page == addNoteUrl ? 'class="active"' : '' }><a href="${addNoteUrl}">Add Note</a></li>
                    </sec:authorize>
                    <c:url var="profileUrl" value="/profile"/>
                    <li id="nav-profile" ${page == profileUrl ? 'class="active"' : '' }><a href="${profileUrl}">Profile</a></li>
                    <c:url var="logoutUrl" value="/logout"/>
                    <li id="nav-logout"><a href="${logoutUrl}">Logout</a></li>
                </c:if>
                <c:if test="${not isAuthenticated}">
                    <li id="nav-login" class="active"><a href="<c:if test="${page != '/sign-in'}">/sign-in</c:if>#">Sign in</a></li>
                    <li id="nav-registration"><a href="<c:if test="${page != '/sign-in'}">/sign-in</c:if>#registration">Sign up</a></li>
                </c:if>
            </ul>
        </div>
    </div>
</nav>

<%--${param.param1}--%>
<%--${pageContext.request.requestURL}--%>
<%--requestScope['javax.servlet.forward.request_uri']--%>
