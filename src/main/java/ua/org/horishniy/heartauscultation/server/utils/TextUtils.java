package ua.org.horishniy.heartauscultation.server.utils;

import org.jetbrains.annotations.Nullable;

/**
 * Created by rage on 12.03.16.
 */
public class TextUtils {

	public static boolean isEmpty(@Nullable CharSequence str) {
		if (str == null || str.length() == 0)
			return true;
		else
			return false;
	}

	public static boolean equals(@Nullable CharSequence a, @Nullable CharSequence b) {
		if (a == b) return true;
		int length;
		if (a != null && b != null && (length = a.length()) == b.length()) {
			if (a instanceof String && b instanceof String) {
				return a.equals(b);
			} else {
				for (int i = 0; i < length; i++) {
					if (a.charAt(i) != b.charAt(i)) return false;
				}
				return true;
			}
		}
		return false;
	}

	public static String removeSpaces(@Nullable String string) {
		if (string == null) {
			return null;
		}
		return  string.trim().replaceAll(" +", " ");
	}
}
