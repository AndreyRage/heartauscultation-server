package ua.org.horishniy.heartauscultation.server.dao;

import org.springframework.stereotype.Repository;
import ua.org.horishniy.heartauscultation.server.entities.Drug;

/**
 * Created by rage on 10.03.16.
 */
@Repository("drugDao")
public class DrugDaoImpl extends AbstractDao<Long, Drug> implements DrugDao {

	public DrugDaoImpl() {
		super(Drug.class);
	}
}
