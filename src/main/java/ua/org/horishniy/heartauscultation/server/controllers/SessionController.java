package ua.org.horishniy.heartauscultation.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.org.horishniy.heartauscultation.server.entities.User;
import ua.org.horishniy.heartauscultation.server.enums.Role;
import ua.org.horishniy.heartauscultation.server.exceptions.BadRequestException;
import ua.org.horishniy.heartauscultation.server.service.UserService;
import ua.org.horishniy.heartauscultation.server.service.entities.responses.CheckUsernameResponse;
import ua.org.horishniy.heartauscultation.server.service.entities.responses.ErrorResponse;
import ua.org.horishniy.heartauscultation.server.utils.TextUtils;
import ua.org.horishniy.heartauscultation.server.utils.ValidationUtils;

import java.nio.charset.Charset;

/**
 * Created by rage on 30.03.16.
 */
@Controller
public class SessionController {

	@Autowired
	private UserService mUserService;

	@Autowired
	private AuthenticationManager mAuthenticationManager;

	@RequestMapping(value = "/api/login", method = RequestMethod.POST)
	public ResponseEntity login(@RequestHeader(name = "Authorization") String authorization) {
		if (authorization != null && authorization.startsWith("Basic")) {
			String base64Credentials = authorization.substring("Basic".length()).trim();
			String credentials = new String(Base64Utils.decode(base64Credentials.getBytes()), Charset.forName("UTF-8"));
			final String[] values = credentials.split(":", 2);

			Authentication authToken = new UsernamePasswordAuthenticationToken(values[0], values[1]);
			Authentication auth = mAuthenticationManager.authenticate(authToken);
			SecurityContextHolder.getContext().setAuthentication(auth);

			//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			return ResponseEntity.ok().body((User) auth.getPrincipal());
		} else {
			throw new BadRequestException("Bad request");
		}
	}

	@RequestMapping(value = {"api/register"}, method = RequestMethod.POST)
	public ResponseEntity register(@RequestHeader("name") String name,
								   @RequestHeader("password") String password,
								   @RequestHeader("rePassword") String rePassword,
								   @RequestHeader("email") String email,
								   @RequestHeader("role") String roleHeader) {
		Role role;
		if ("ADMIN".equalsIgnoreCase(roleHeader)) {
			role = Role.ADMIN;
		} else if ("MODERATOR".equalsIgnoreCase(roleHeader)) {
			role = Role.MODERATOR;
		} else if ("DOCTOR".equalsIgnoreCase(roleHeader)) {
			role = Role.DOCTOR;
		} else if ("PATIENT".equalsIgnoreCase(roleHeader)) {
			role = Role.PATIENT;
		} else {
			throw new BadRequestException("Bad role");
		}

		//Check permission for create super user
		if (role == Role.ADMIN || role == Role.MODERATOR) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth == null || auth.getAuthorities() == null || !auth.getAuthorities().contains(Role.ADMIN)) {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ErrorResponse("Permission denied"));
			}
		}

		if (!ValidationUtils.isPasswordValid(password)) {
			throw new BadRequestException("Password is too short (minimum is 6 characters)");
		}
		if (!TextUtils.equals(password, rePassword)) {
			throw new BadRequestException("Password not match");
		}

		PasswordEncoder encoder = new BCryptPasswordEncoder();
		User user = new User(name, encoder.encode(password), email, role);

		mUserService.add(user);

		Authentication currentAuth = SecurityContextHolder.getContext().getAuthentication();
		User createdUser;
		if (currentAuth == null || currentAuth.getPrincipal().equals("anonymousUser")) {
			Authentication authToken = new UsernamePasswordAuthenticationToken(name, password);
			Authentication auth = mAuthenticationManager.authenticate(authToken);
			SecurityContextHolder.getContext().setAuthentication(auth);
			createdUser = (User) auth.getPrincipal();
		} else {
			createdUser = (User) mUserService.loadUserByUsername(name);
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
	}

	@RequestMapping(value = {"api/check-username"}, method = RequestMethod.GET)
	public ResponseEntity<CheckUsernameResponse> checkUsername(@RequestParam("username") String username) {
		boolean isExists = mUserService.isUserNameAlreadyExists(username);
		return ResponseEntity.ok().body(new CheckUsernameResponse(isExists));
	}
}
