<%--
  Created by IntelliJ IDEA.
  User: rage
  Date: 30.03.16
  Time: 13:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/navbar.jsp"/>

<div class="container">
    <div id="login" class="form-style">
        <div class="container-body">
            <c:url var="loginUrl" value="/sign-in"/>
            <form action="${loginUrl}" method="post" class="form-signin">

                <h2 class="form-signin-heading">Please sign in</h2>

                <div id="login-alert">
                    <c:if test="${param.error != null}">
                        <div id="invalid-credentials" class="alert alert-danger">
                            <p>The username or password is incorrect.</p>
                        </div>
                    </c:if>
                    <c:if test="${param.logout != null}">
                        <div id="logout-successfully" class="alert alert-success">
                            <p>You have been logged out successfully.</p>
                        </div>
                    </c:if>
                </div>

                <div class="input-group top-field-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <label for="login-username" class="sr-only">Username</label>
                    <input type="text" id="login-username" class="form-control top-field" name="username" placeholder="Username"
                           value="${param.logout}" required autofocus>
                </div>

                <div class="input-group bottom-field-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <label for="login-password" class="sr-only">Password</label>
                    <input type="password" id="login-password" class="form-control bottom-field" name="password" placeholder="Password"
                           required>
                </div>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div>
    </div>

    <div id="registration" class="form-style">
        <div class="container-body">
            <form class="form-signup">

                <h2 class="form-signin-heading">Registration</h2>

                <div id="register-alert"></div>

                <div class="input-group top-field-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <label for="register-username" class="sr-only">Username</label>
                    <input type="text" id="register-username" class="form-control top-field" name="username" placeholder="Username"
                           value="${param.logout}" required autofocus>
                </div>

                <div class="input-group center-field-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <label for="register-email" class="sr-only">E-mail</label>
                    <input type="text" id="register-email" class="form-control center-field" name="email" placeholder="E-mail" required>
                </div>

                <div class="input-group center-field-group selector-field-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></span>
                    <label for="register-role" class="sr-only">Role</label>
                    <select id="register-role" class="form-control center-field form-select" name="role">
                        <option>Patient</option>
                        <option>Doctor</option>
                    </select>
                </div>

                <div class="input-group center-field-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <label for="register-password" class="sr-only">Password</label>
                    <input type="password" id="register-password" class="form-control center-field" name="password" placeholder="Password"
                           required>
                </div>

                <div class="input-group bottom-field-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                    <label for="register-re-password" class="sr-only">Repeat password</label>
                    <input type="password" id="register-re-password" class="form-control bottom-field" name="re-password" placeholder="Repeat password"
                           required>
                </div>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <button class="btn btn-lg btn-primary btn-block btn-register" type="submit">Sign up</button>
            </form>
        </div>
    </div>

</div>

<script src="<c:url value='/static/jquery/jquery-1.12.2.min.js' />"></script>
<script>window.jQuery || document.write('<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"><\/script>')</script>
<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js' />"></script>
<script src="<c:url value='/static/js/sign-in.js' />"></script>
<script src="<c:url value='/static/js/utils.js' />"></script>

<jsp:include page="include/footer.jsp"/>